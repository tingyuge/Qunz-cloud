from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^register', views.register, name='register'),
    url(r'^login', views.login, name='login'),
    url(r'^logout', views.logout, name='logout'),
    url(r'amount', views.amount, name='amount'),
    url(r'user_detail', views.user_detail, name='user_detail'),
    # url(r'^forgot_reset/(?P<email>.+)/(?P<uid>\d+)/(?P<method>.+)', views.forgot_reset, name='forgot_reset'),
    url(r'^mailbox/(?P<email>.+)/(?P<uid>\d+)', views.mailbox, name='mailbox'),
    # url(r'^getJoinDetail', views.getJoinDetail, name='getJoinDetail'),

    url(r'^reset', views.VerificationView.as_view(), name='reset'),
    url(r'real_name', views.real_name, name='real_name'),
]
