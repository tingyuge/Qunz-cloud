# -*- coding: utf-8 -*-

from django.apps import AppConfig

class PeopleConfig(AppConfig):
    name = 'people'
    verbose_name = "用户"