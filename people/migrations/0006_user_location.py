# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0005_user_station'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='location',
            field=models.CharField(default=b'', max_length=50, null=True, verbose_name=b'\xe6\x89\x80\xe5\x9c\xa8\xe5\x9c\xb0'),
        ),
    ]
