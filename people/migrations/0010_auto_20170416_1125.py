# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0009_auto_20170405_2116'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprivate',
            old_name='bitcoin',
            new_name='bit_coin',
        ),
        migrations.RenameField(
            model_name='userprivate',
            old_name='photoback',
            new_name='photo_back',
        ),
        migrations.RenameField(
            model_name='userprivate',
            old_name='photofont',
            new_name='photo_front',
        ),
    ]
