# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0004_auto_20160417_0956'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='station',
            field=models.IntegerField(default=1, verbose_name=b'\xe5\xb8\x90\xe5\x8f\xb7\xe7\x8a\xb6\xe6\x80\x81'),
        ),
    ]
