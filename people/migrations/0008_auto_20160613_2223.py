# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0007_auto_20160510_2309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='birthday',
            field=models.DateField(null=True, verbose_name=b'\xe7\x94\x9f\xe6\x97\xa5', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='brief',
            field=models.CharField(max_length=200, null=True, verbose_name=b'\xe7\xae\x80\xe4\xbb\x8b', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=30, unique=True, null=True, verbose_name=b'\xe9\x82\xae\xe7\xae\xb1', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='favourit',
            field=models.CharField(max_length=100, null=True, verbose_name=b'\xe7\x88\xb1\xe5\xa5\xbd', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='idnum',
            field=models.CharField(max_length=50, null=True, verbose_name=b'\xe8\xba\xab\xe4\xbb\xbd\xe8\xaf\x81\xe5\x8f\xb7\xe7\xa0\x81', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='isreal',
            field=models.FileField(max_length=255, upload_to=b'static/verify/%Y%m%d', null=True, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe5\xae\x9e\xe5\x90\x8d', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='name',
            field=models.CharField(max_length=10, null=True, verbose_name=b'\xe5\xa7\x93\xe5\x90\x8d', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='nicklogo',
            field=models.FileField(upload_to=b'static/nicklogo/%Y%m%d', max_length=255, verbose_name=b'\xe7\x94\xa8\xe6\x88\xb7\xe5\xa4\xb4\xe5\x83\x8f', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='occupation',
            field=models.CharField(max_length=30, null=True, verbose_name=b'\xe8\x81\x8c\xe4\xb8\x9a', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='photoback',
            field=models.FileField(max_length=255, upload_to=b'static/verify/%Y%m%d', null=True, verbose_name=b'\xe8\xba\xab\xe4\xbb\xbd\xe8\xaf\x81\xe5\x8f\x8d\xe9\x9d\xa2', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='province',
            field=models.CharField(default=b'', max_length=50, null=True, verbose_name=b'\xe6\x89\x80\xe5\x9c\xa8\xe5\x9c\xb0', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='salt',
            field=models.CharField(max_length=40, verbose_name=b'\xe7\x9b\x90'),
        ),
        migrations.AlterField(
            model_name='user',
            name='sex',
            field=models.IntegerField(default=0, verbose_name=b'\xe6\x80\xa7\xe5\x88\xab', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='third_party_account',
            field=models.CharField(max_length=50, null=True, verbose_name=b'\xe7\xac\xac\xe4\xb8\x89\xe6\x96\xb9\xe8\xb4\xa6\xe5\x8f\xb7', blank=True),
        ),
    ]
