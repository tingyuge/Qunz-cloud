# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0002_user_brief'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': '\u7528\u6237', 'verbose_name_plural': '\u7528\u6237'},
        ),
        migrations.RemoveField(
            model_name='userprivate',
            name='photo',
        ),
        migrations.AddField(
            model_name='user',
            name='photoback',
            field=models.FileField(max_length=255, null=True, verbose_name=b'\xe8\xba\xab\xe4\xbb\xbd\xe8\xaf\x81\xe5\x8f\x8d\xe9\x9d\xa2', upload_to=b'verify/%Y%m%d'),
        ),
        migrations.AddField(
            model_name='userprivate',
            name='photoback',
            field=models.FileField(default=None, max_length=255, upload_to=b'verify/%Y%m%d'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userprivate',
            name='photofont',
            field=models.FileField(default=None, max_length=255, upload_to=b'verify/%Y%m%d'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='user',
            name='birthday',
            field=models.DateField(null=True, verbose_name=b'\xe7\x94\x9f\xe6\x97\xa5'),
        ),
        migrations.AlterField(
            model_name='user',
            name='brief',
            field=models.CharField(max_length=200, null=True, verbose_name=b'\xe7\xae\x80\xe4\xbb\x8b'),
        ),
        migrations.AlterField(
            model_name='user',
            name='complete',
            field=models.IntegerField(default=0, verbose_name=b'\xe5\xae\x8c\xe6\x88\x90\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x95\xb0'),
        ),
        migrations.AlterField(
            model_name='user',
            name='create_date',
            field=models.DateTimeField(null=True, verbose_name=b'\xe6\xb3\xa8\xe5\x86\x8c\xe6\x97\xa5\xe6\x9c\x9f'),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=30, unique=True, null=True, verbose_name=b'\xe9\x82\xae\xe7\xae\xb1'),
        ),
        migrations.AlterField(
            model_name='user',
            name='email_verify',
            field=models.IntegerField(default=0, verbose_name=b'\xe5\xaf\x86\xe7\xa0\x81\xe9\xaa\x8c\xe8\xaf\x81\xe7\x8a\xb6\xe6\x80\x81'),
        ),
        migrations.AlterField(
            model_name='user',
            name='favourit',
            field=models.CharField(max_length=100, null=True, verbose_name=b'\xe7\x88\xb1\xe5\xa5\xbd'),
        ),
        migrations.AlterField(
            model_name='user',
            name='idnum',
            field=models.CharField(max_length=50, null=True, verbose_name=b'\xe8\xba\xab\xe4\xbb\xbd\xe8\xaf\x81\xe5\x8f\xb7\xe7\xa0\x81'),
        ),
        migrations.AlterField(
            model_name='user',
            name='isreal',
            field=models.FileField(max_length=255, null=True, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe5\xae\x9e\xe5\x90\x8d', upload_to=b'verify/%Y%m%d'),
        ),
        migrations.AlterField(
            model_name='user',
            name='lasttime',
            field=models.DateTimeField(null=True, verbose_name=b'\xe6\x9c\x80\xe5\x90\x8e\xe4\xb8\x80\xe6\xac\xa1\xe7\x99\xbb\xe5\xbd\x95\xe6\x97\xb6\xe9\x97\xb4'),
        ),
        migrations.AlterField(
            model_name='user',
            name='name',
            field=models.CharField(max_length=10, null=True, verbose_name=b'\xe5\xa7\x93\xe5\x90\x8d'),
        ),
        migrations.AlterField(
            model_name='user',
            name='nicklogo',
            field=models.FileField(upload_to=b'upload/%Y%m%d', max_length=255, verbose_name=b'\xe7\x94\xa8\xe6\x88\xb7\xe5\xa4\xb4\xe5\x83\x8f'),
        ),
        migrations.AlterField(
            model_name='user',
            name='nickname',
            field=models.CharField(max_length=30, verbose_name=b'\xe6\x98\xb5\xe7\xa7\xb0'),
        ),
        migrations.AlterField(
            model_name='user',
            name='occupation',
            field=models.CharField(max_length=30, null=True, verbose_name=b'\xe8\x81\x8c\xe4\xb8\x9a'),
        ),
        migrations.AlterField(
            model_name='user',
            name='password',
            field=models.CharField(max_length=40, verbose_name=b'\xe5\xaf\x86\xe7\xa0\x81'),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(max_length=12, unique=True, null=True, verbose_name=b'\xe6\x89\x8b\xe6\x9c\xba\xe5\x8f\xb7'),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone_verify',
            field=models.IntegerField(default=0, verbose_name=b'\xe6\x89\x8b\xe6\x9c\xba\xe9\xaa\x8c\xe8\xaf\x81\xe7\x8a\xb6\xe6\x80\x81'),
        ),
        migrations.AlterField(
            model_name='user',
            name='reputation',
            field=models.FloatField(default=0, verbose_name=b'\xe5\xa3\xb0\xe8\xaa\x89'),
        ),
        migrations.AlterField(
            model_name='user',
            name='sex',
            field=models.IntegerField(default=0, verbose_name=b'\xe6\x80\xa7\xe5\x88\xab'),
        ),
        migrations.AlterField(
            model_name='user',
            name='third_party_account',
            field=models.CharField(max_length=50, null=True, verbose_name=b'\xe7\xac\xac\xe4\xb8\x89\xe6\x96\xb9\xe8\xb4\xa6\xe5\x8f\xb7'),
        ),
    ]
