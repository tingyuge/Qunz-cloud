# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone', models.CharField(max_length=12, unique=True, null=True)),
                ('email', models.EmailField(max_length=30, unique=True, null=True)),
                ('password', models.CharField(max_length=40)),
                ('salt', models.CharField(max_length=40)),
                ('nickname', models.CharField(max_length=30)),
                ('nicklogo', models.FileField(max_length=255, upload_to=b'upload/%Y%m%d')),
                ('isreal', models.FileField(max_length=255, upload_to=b'verify/%Y%m%d')),
                ('idnum', models.CharField(max_length=50, null=True)),
                ('name', models.CharField(max_length=10, null=True)),
                ('sex', models.IntegerField(default=0)),
                ('third_party_account', models.CharField(max_length=50, null=True)),
                ('birthday', models.DateField(null=True)),
                ('occupation', models.CharField(max_length=30, null=True)),
                ('favourit', models.CharField(max_length=100, null=True)),
                ('reputation', models.FloatField(default=0)),
                ('complete', models.IntegerField(default=0)),
                ('phone_verify', models.IntegerField(default=0)),
                ('email_verify', models.IntegerField(default=0)),
                ('create_date', models.DateTimeField(null=True)),
                ('lasttime', models.DateTimeField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserPrivate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cash', models.FloatField(default=0)),
                ('bitcoin', models.FloatField(default=0)),
                ('id_card', models.CharField(max_length=50)),
                ('photo', models.CharField(max_length=255)),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
    ]
