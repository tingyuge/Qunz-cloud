# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0010_auto_20170416_1125'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='isreal',
        ),
        migrations.RemoveField(
            model_name='user',
            name='photoback',
        ),
        migrations.AddField(
            model_name='user',
            name='is_real',
            field=models.BooleanField(default=False, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe5\xae\x9e\xe5\x90\x8d'),
        ),
    ]
