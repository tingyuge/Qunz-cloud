# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0003_auto_20160416_2130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='isreal',
            field=models.FileField(max_length=255, null=True, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe5\xae\x9e\xe5\x90\x8d', upload_to=b'static/verify/%Y%m%d'),
        ),
        migrations.AlterField(
            model_name='user',
            name='nicklogo',
            field=models.FileField(upload_to=b'static/nicklogo/%Y%m%d', max_length=255, verbose_name=b'\xe7\x94\xa8\xe6\x88\xb7\xe5\xa4\xb4\xe5\x83\x8f'),
        ),
        migrations.AlterField(
            model_name='user',
            name='photoback',
            field=models.FileField(max_length=255, null=True, verbose_name=b'\xe8\xba\xab\xe4\xbb\xbd\xe8\xaf\x81\xe5\x8f\x8d\xe9\x9d\xa2', upload_to=b'static/verify/%Y%m%d'),
        ),
        migrations.AlterField(
            model_name='userprivate',
            name='photoback',
            field=models.FileField(max_length=255, upload_to=b'static/verify/%Y%m%d'),
        ),
        migrations.AlterField(
            model_name='userprivate',
            name='photofont',
            field=models.FileField(max_length=255, upload_to=b'static/verify/%Y%m%d'),
        ),
    ]
