# -*- coding: utf-8 -*-
from django.db import models
from django.forms.models import model_to_dict

# Create your models here.


class User(models.Model):
    class Meta:
        verbose_name = '用户'
        verbose_name_plural = '用户'

    # 用户手机号
    phone = models.CharField('手机号', null=True, max_length=12, unique=True)
    # 用户邮箱
    email = models.EmailField('邮箱', null=True, max_length=30, unique=True)
    password = models.CharField('密码', max_length=40)
    salt = models.CharField('盐', max_length=40)
    nickname = models.CharField('昵称', max_length=30)
    # 用户头像
    nicklogo = models.FileField('用户头像', upload_to="static/nicklogo/%Y%m%d", max_length=255, blank=True)
    # 是否实名，False为未认证
    is_real = models.BooleanField('是否实名', default=False)
    # 身份证号码
    idnum = models.CharField('身份证号码', null=True, max_length=50, blank=True)
    # 姓名
    name = models.CharField('姓名', null=True, max_length=10, blank=True)
    # 性别
    sex = models.IntegerField('性别', default=0, blank=True)
    # 地点
    province = models.CharField('所在地', max_length=50, null=True, default='', blank=True)
    # 第三方帐号
    third_party_account = models.CharField('第三方账号', null=True, max_length=50, blank=True)
    # 生日
    birthday = models.DateField('生日', null=True, blank=True)
    # 职业
    occupation = models.CharField('职业', null=True, max_length=30, blank=True)
    # 爱好
    favourit = models.CharField('爱好', null=True, max_length=100, blank=True)
    # 简介
    brief = models.CharField('简介', null=True, max_length=200, blank=True)
    # 声誉
    reputation = models.FloatField('声誉', default=0)
    # 完成任务数
    complete = models.IntegerField('完成任务数', default=0)
    # 手机号验证，0为未验证
    phone_verify = models.IntegerField('手机验证状态', default=0)
    # 邮箱验证，0为未验证
    email_verify = models.IntegerField('密码验证状态', default=0)
    # 帐号注册日期
    create_date = models.DateTimeField('注册日期', null=True)
    # 最后一次登录时间
    lasttime = models.DateTimeField('最后一次登录时间', null=True)
    # 帐号状态,0被封号1正常
    station = models.IntegerField('帐号状态', default=1)

    def __unicode__(self):
        return self.nickname

    def get_user(self):
        user_value = model_to_dict(self)
        user_value['nicklogo'] = user_value.get('nicklogo').name
        del user_value['password']
        del user_value['salt']
        return user_value


class UserPrivate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # 钱包现金
    cash = models.FloatField(default=0)
    # 智币
    bit_coin = models.FloatField(default=0)
    # 身份证号
    id_card = models.CharField(max_length=50)
    # 实名照片正面
    photo_front = models.FileField(upload_to="static/verify/%Y%m%d", max_length=255)
    # 实名照片反面
    photo_back = models.FileField(upload_to="static/verify/%Y%m%d", max_length=255)

    def reduce_amount(self, reduce_detail):
        self.cash = self.cash - reduce_detail.get('cash', 0)
        self.bit_coin = self.bit_coin - reduce_detail.get('bit_coin', 0)
        self.save()
