# -*- coding: utf-8 -*-
from django.contrib import admin

# Register your models here.

from models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['nickname', 'email', 'province']
    search_fields = ('nickname',)
    exclude = ('salt', 'third_party_account')

