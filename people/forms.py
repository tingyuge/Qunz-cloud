# -*- coding: utf-8 -*-
import hashlib
import re
import datetime
import pytz

from django import forms
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _
from django.forms.utils import ValidationError

from common import functions
from utils.code_msg import codeMsg

from models import User, UserPrivate


class UserForm(forms.ModelForm):
    username = forms.CharField(max_length=30)
    phone = forms.CharField(required=False, max_length=30)
    email = forms.CharField(required=False, max_length=30)
    password_verify = forms.CharField(max_length=30)
    create_date = forms.DateTimeField(required=False)

    class Meta:
        fields = ['phone', 'email', 'password', 'nickname', 'create_date']
        model = User

    def clean(self):
        # run the standard clean method first
        cleaned_data = super(UserForm, self).clean()

        password = cleaned_data.get("password")
        password_verify = cleaned_data.get("password_verify")
        # check if passwords are entered and match
        if password and password_verify and password == password_verify:
            cleaned_data['password'] = hashlib.sha1(password).hexdigest()
        else:
            self.add_error('password_verify',
                           ValidationError(_('password_verify'), code='password_mot_match'))

        cleaned_data['create_date'] = datetime.datetime.now(pytz.timezone('Asia/Shanghai'))

        username = functions.match_num(cleaned_data.get('username'))
        if username and username.get('key') == 'email':
            cleaned_data['email'] = username.get('data')
            cleaned_data['username'] = username.get('key')
        elif username and username.get('key') == 'phone':
            cleaned_data['phone'] = username.get('data')
            cleaned_data['username'] = username.get('key')
        else:
            self.add_error('username', ValidationError(_('username'), code='username_empty'))

        # always return the cleaned data
        return cleaned_data


class ResetMailPassForm(forms.ModelForm):
    email = forms.EmailField(max_length=30, required=True)
    password = forms.CharField(max_length=255, required=True)
    password_verify = forms.CharField(max_length=255, required=True)

    class Meta:
        fields = ['email', 'password']
        model = User

    def clean(self):
        cleaned_data = super(ResetMailPassForm, self).clean()

        password = cleaned_data.get('password')
        password_verify = cleaned_data.get('password_verify')
        if password and password_verify and password == password_verify:
            cleaned_data['password'] = hashlib.sha1(password).hexdigest()
        else:
            self.add_error('password', ValidationError(_('password_verify'), code='not_match'))

        return cleaned_data


class UserVerifyForm(forms.ModelForm):
    """
    实名认证form
    """
    idnum = forms.CharField(max_length=50)
    is_real = forms.FileField(required=True)

    class Meta:
        fields = ['id', 'is_real', 'idnum']
        model = User


class UserRealNameVerifyForm(forms.ModelForm):
    """
    实名验证Form
    """
    id_card = forms.CharField(max_length=50)
    photo_front = forms.FileField(required=True)
    photo_back = forms.FileField(required=True)

    class Meta:
        fields = ['user', 'id_card', 'photo_back', 'photo_front']
        model = UserPrivate


class ContactVerifyForm(forms.ModelForm):
    username = forms.CharField(max_length=255)
    method = forms.CharField()
    email_verify = forms.IntegerField(required=False)
    phone_verify = forms.IntegerField(required=False)

    class Meta:
        fields = ['id', 'email_verify', 'phone_verify']
        model = User

    def clean(self):
        cleaned_data = super(ContactVerifyForm, self).clean()

        method = cleaned_data.get('method')
        if method == 'email':
            cleaned_data['email_verify'] = 1
        elif method == 'phone':
            cleaned_data['phone_verify'] = 1
        else:
            self.add_error('username', ValidationError(_('no username'), code='not_username'))
        return cleaned_data

