from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^checkUpdate', views.checkUpdate, name='checkUpdate'),
    url(r'^getIndustry', views.getIndustry, name='getIndustry'),
]
