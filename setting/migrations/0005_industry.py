# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('setting', '0004_delete_industry'),
    ]

    operations = [
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('describe', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': '\u884c\u4e1a',
                'verbose_name_plural': '\u884c\u4e1a',
            },
        ),
    ]
