# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('setting', '0006_sensor'),
    ]

    operations = [
        migrations.CreateModel(
            name='Versions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version', models.CharField(max_length=32)),
                ('describe', models.CharField(max_length=1024)),
                ('downloadurl', models.CharField(max_length=255)),
                ('downloadnum', models.IntegerField(default=0)),
            ],
        ),
    ]
