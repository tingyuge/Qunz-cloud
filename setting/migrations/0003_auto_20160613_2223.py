# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('setting', '0002_industry'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='industry',
            options={'verbose_name': '\u884c\u4e1a', 'verbose_name_plural': '\u884c\u4e1a'},
        ),
    ]
