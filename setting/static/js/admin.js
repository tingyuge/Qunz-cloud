var mission_auto_check = function() {
    var right_column = document.querySelector('.inner-right-column');

    var check_msg_box = document.createElement('div');
    right_column.appendChild(check_msg_box);
    check_msg_box.classList.add('box')

    check_msg_box.innerHTML = "<h4 class=\"italic-title\">自动检测</h4>";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        var p = document.createElement('p');
        if (xhr.readyState == 4 && xhr.status == 200) {
            res_obj = JSON.parse(xhr.responseText);
            var result = JSON.parse(res_obj.result);
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var p = document.createElement('p');
                p.innerHTML = result[i][0];
                check_msg_box.appendChild(p);
            }
        }
        else {
            console.log('');
        }
        check_msg_box.appendChild(p)
    }

    var type = window.location.pathname.split('/')[3]
    var id = window.location.pathname.split('/')[4]
    xhr.open('GET', '/task/task_auto_check?type=' + type + '&id=' + id, true);
    xhr.send();
}

var init_check_page = function(task_type){
    // var run_task_process = document.querySelector('button[name=_addanother]');
    // run_task_process.type = 'button';
    // run_task_process.innerHTML = '运行算法';
    var task_id = window.location.href.split('/')[6];
    
    var save = document.querySelector('button[name=_save]');
    save.innerHTML = '确认';
    
    var save_and_next = document.querySelector('button[name=_continue]');
    save_and_next.name = '_save';
    save_and_next.innerHTML = '待确认';
    
    var station_select = document.querySelector('select[name=station]');
    
    var pass_code = task_type == 'task'? 4:6
    var refuse_code = task_type == 'task'? 2:4
    
    // run_task_process.onclick = function () {
    //     $.get('/task/run_process/'+task_id, function(data){
    //         if (data.statu == 200){
    //             run_task_process.innerHTML = "已运行";
    //         }
    //     })
    // }
    save.onclick = function(){
        station_select.value = pass_code;
    };
    save_and_next.onclick = function() {
        station_select.value = refuse_code;
    };
};

var init_wenjurn = function() {
    var wenjuan_parser = function (data) {
        wj_obj = JSON.parse(data);
        var wj_html = "";
        for (var a = 0; a < wj_obj.length; a ++){
            wj_html += "<tr><td>" + wj_obj[a].question + "</td></tr>"
        }
        return '<table class="table table-striped">' + wj_html + '</table>'
    };
    var save = document.querySelector('button[name=_save]');
    save.innerHTML = '确认';
    
    var save_and_next = document.querySelector('button[name=_continue]');
    save_and_next.name = '_save';
    save_and_next.innerHTML = '待确认';

    var w_content = document.querySelector('input[name="content"]');
    w_content.style.display = 'none';

    var w_div = document.createElement('div');
    w_div.innerHTML = wenjuan_parser(w_content.value);

    w_content.parentNode.appendChild(w_div);
};

var set_download_link = function(){
    var files = document.getElementsByClassName('file-upload');
    for (var i = 0; i < files.length; i++){
        var myRe = new RegExp(".*(static/.*)", "g");
        var a = files[i].querySelector('a');
        var real_link = myRe.exec(a.href);
        if (real_link){
            a.href = '/' + real_link[1];
        }
    }
}

var set_to_cn = function() {
    var selector = document.querySelector('#changelist-search > div > span > select:nth-child(2) > option:nth-child(1)')
    if (selector) {
        selector.innerHTML = '管理员'
    }
    
    var actionlist = document.querySelector('.actionlist').childNodes
    if (actionlist){
        for (var i = 0; i < actionlist.length; i ++) {
            var text = actionlist[i].innerText;
            if (text) {
                text = text.replace('Changed', '修改');
                text = text.replace('Added', '增加')
                text = text.replace('Deleted', '删除')
            }
            actionlist[i].innerText = text;
        }
    }
}


window.onload = function() {

    var is_admin = document.getElementById('user-tools').innerText.includes('admin');
    
    if (window.location.pathname.match('task/task/') && !is_admin){
        mission_auto_check();
        init_check_page('task');
    }
    if (window.location.pathname.match('task/datatask/') && !is_admin){
        init_check_page('datatask');
    }
    if (window.location.pathname.match('admin/logentry/')){
        set_to_cn();
    }
    if (window.location.pathname == '/admin/'){
        set_to_cn();
    }
    if (window.location.pathname.match('wenjuan/question')) {
        init_wenjurn();
    }
    set_download_link();
}
