# -*- coding: utf-8 -*-
from django.db import models

import json

# Create your models here.


class Options(models.Model):
    option_key = models.CharField(max_length=64)
    option_value = models.CharField(max_length=1000)


class Industry(models.Model):
    class Meta:
        verbose_name = '行业'
        verbose_name_plural = '行业'
    name = models.CharField(max_length=255)
    describe = models.CharField(max_length=255)


class Sensor(models.Model):
    name = models.CharField(max_length=255)
    describe = models.CharField(max_length=255)


class Versions(models.Model):
    '''
    客户端版本
    '''
    # 版本号
    version = models.CharField(max_length=32)
    # 更新简介
    describe = models.CharField(max_length=1024)
    # 下载链接
    downloadurl = models.CharField(max_length=255)
    # 下载数量
    downloadnum = models.IntegerField(default=0)

    def toDict(self):
        fields = []
        for field in self._meta.fields:
            fields.append(field.name)

        d = {}
        for attr in fields:
            d[attr] = getattr(self, attr)

        return d
