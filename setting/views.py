# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Options, Industry, Versions
from django.core import serializers

import json
# Create your views here.


# 检查更新接口
def checkUpdate(request):
    if request.method == 'POST':
        edition = request.POST['edition']
        item = Versions.objects.last()
        print item
        version = item.version
        if edition < version:
            return JsonResponse({'status': 'success', 'msg': item.toDict()})
        else:
            return JsonResponse({'status': 'success', 'msg': 'error'})
    return JsonResponse({'status': 'error', 'msg': ''})


def readMe(request):
    # 返回一个web页面的链接
    pass


def feedBack(request):
    if request.method == 'POST':
        return JsonResponse({'status': 'success', 'msg': ''})


def getIndustry(request):
    industry = serializers.serialize("json", Industry.objects.all())
    industry = json.loads(industry)
    return JsonResponse({'status': 'success', 'msg': industry})


def downNest(request):
    # 暂时不需要实现，考虑使用七牛等云存储
    pass
