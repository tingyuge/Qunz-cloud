var get_data = function () {
    var log = function(type, data){console.log('received data:'+type); console.log(data)};
    $.get('/static/china.json', function (chinaJson) {
        echarts.registerMap('china', chinaJson);
    });
    $.get('/analyse/api/user_location', function (data, status) {
        if (status == 'success') {
            setup_userlocation_chart(data);
            log('user location', data);
        }
    });
    $.get('/analyse/api/user_age', function (data, status) {
        if (status == 'success') {
            setup_userage_chart(data);
            log('user age', data);
        }
    });
    $.get('/analyse/api/money_exchange', function(data, status){
        if (status == 'success') {
            setup_exchange_chart(data);
            log('money exchange', data);
        }
    })
}

var setup_userlocation_chart = function (data) {
    var location_option = {
        title: {
            text: '地域分布',
            subtext: '',
            left: 'center'
        },
        tooltip: {
            trigger: 'item'
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: ['发布者', '众客']
        },
        visualMap: {
            min: 0,
            max: 2500,
            left: 'left',
            top: 'bottom',
            text: ['高', '低'],           // 文本，默认为数值文本
            calculable: true
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            left: 'right',
            top: 'center',
            feature: {
                dataView: { readOnly: false },
                restore: {},
                saveAsImage: {}
            }
        },
        series: [
            {
                name: '发布者',
                type: 'map',
                mapType: 'china',
                roam: false,
                label: {
                    normal: {
                        show: true
                    },
                    emphasis: {
                        show: true
                    }
                },
                data: data['publisher']
            },
            {
                name: '众客',
                type: 'map',
                mapType: 'china',
                label: {
                    normal: {
                        show: true
                    },
                    emphasis: {
                        show: true
                    }
                },
                data: data['worker']
            }
        ]
    };

    var location_chart = echarts.init(document.getElementById('location-chart'));
    location_chart.setOption(location_option);
}

var setup_userage_chart = function (data) {
    var age_option = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data: ['发布者-男', '发布者-女', '众客-男', '众客-女']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: data['x_axis']
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '发布者-男',
                type: 'bar',
                data: data['publisher-male']
            },
            {
                name: '发布者-女',
                type: 'bar',
                data: data['publisher-female']
            },
            {
                name: '众客-男',
                type: 'bar',
                data: data['worker-male']
            },
            {
                name: '众客-女',
                type: 'bar',
                data: data['worker-female']
            },
        ]
    };

    var age_chart = echarts.init(document.getElementById('age-chart'));
    age_chart.setOption(age_option);
}

var setup_exchange_chart = function (data) {
    var option = option = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data: ['支付额', '押金额', '费用额']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: data['x_axis']
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '支付额',
                type: 'bar',
                data: data['payment']
            },
            {
                name: '押金额',
                type: 'bar',
                data: data['cash']
            },
            {
                name: '费用额',
                type: 'bar',
                data: data['cost']
            },
            // {
            //     name: '提现金额',
            //     type: 'bar',
            //     data: data['to_cash']
            // },
            // {
            //     name: '智币金额',
            //     type: 'bar',
            //     data: data['zhi']
            // }            
        ]
    };


    var chart = echarts.init(document.getElementById('exchange-chart'));
    chart.setOption(option);
}

$(document).ready(function () {
    get_data();
})