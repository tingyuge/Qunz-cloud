# coding=utf-8
from datetime import datetime

from django.shortcuts import render
from django.db.models import Avg, Max, Min, Count

from people.models import User
from task.models import Task, DataTask
# Create your views here.


# vies start
def index(request):
    '''
        analyse index
    '''
    context = {}
    context['task_average_fb'] = Task.objects.all().aggregate(Avg('submitnum'),
                                                              Max('submitnum'),
                                                              Min('submitnum'))
    context['datatask_average_fb'] = DataTask.objects.all().aggregate(Avg('submitnum'),
                                                                      Max('submitnum'),
                                                                      Min('submitnum'))

    context['trade_models'] = []
    trade_models_cn = ['任务金额', '任务押金', '交易金额']
    trade_models_en = ['faretask', 'taskfare', 'tradefare']
    trade_models = Task.objects.filter(trademodel=1) \
                                .aggregate(Avg('faretask'),Min('faretask'),Max('faretask'),
                                           Avg('taskfare'),Min('taskfare'),Max('taskfare'),
                                           Avg('tradefare'),Min('tradefare'),Max('tradefare'))
    trade_models_value = ['faretask', 'taskfare', 'tradefare']
    print "first trade models :", trade_models
    for i in range(3):
        context['trade_models'].append(['悬赏'+trade_models_cn[i],trade_models.get(trade_models_en[i]+'__avg'),
                                        trade_models.get(trade_models_en[i]+'__max'),trade_models.get(trade_models_en[i]+'__min')])
    trade_models = Task.objects.filter(trademodel=2) \
                                .aggregate(Avg('faretask'),Min('faretask'),Max('faretask'),
                                           Avg('taskfare'),Min('taskfare'),Max('taskfare'),
                                           Avg('tradefare'),Min('tradefare'),Max('tradefare'))
    print "second trade models", trade_models
    for i in range(3):
        context['trade_models'].append(['悬赏'+trade_models_cn[i],trade_models.get(trade_models_en[i]+'__avg'),
                                        trade_models.get(trade_models_en[i]+'__max'),trade_models.get(trade_models_en[i]+'__min')])

    return render(request, 'index.html', context)
