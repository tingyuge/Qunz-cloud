from django.conf.urls import url
from . import views
from . import apis

urlpatterns = [
    url(r'^$', views.index, name='analyse-index'),
    
    url(r'^api/user_location', apis.user_location, name='analyse-api-userlocation'),
    url(r'^api/user_age', apis.user_age, name='analyse-api-userage'),
    url(r'^api/money_exchange', apis.money_exchange, name='analyse-money-exchange'),
]
