# -*- coding: utf-8 -*-
from django.contrib import admin

# Register your models here.

from django.contrib.admin.models import LogEntry, DELETION
from django.utils.html import escape
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib import admin


class LogEntryAdmin(admin.ModelAdmin):

    # date_hierarchy = 'action_time'

    readonly_fields = LogEntry._meta.get_all_field_names()

    list_filter = [
        'action_time',
        # AdminUserFilter,
        'user',
        # 'content_type',
        # 'action_flag'
    ]



    list_display = [
        'action_time',
        'user_view',
        'content_type_view',
        'object_link',
        # 'action_flag',
        # 'change_message',
    ]
    def user_view(self, obj):
        return obj.user
    user_view.short_description = '审核员'
    
    def content_type_view(self, obj):
        return obj.content_type
    content_type_view.short_description = '类型'

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != 'POST'

    def has_delete_permission(self, request, obj=None):
        return False

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link
    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'详细内容'
    
    def queryset(self, request):
        return super(LogEntryAdmin, self).queryset(request) \
            .prefetch_related('content_type')
            
    actions_on_top = False
    actions_on_bottom = False


admin.site.register(LogEntry, LogEntryAdmin)