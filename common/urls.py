from django.conf.urls import url
from . import views
import tests

urlpatterns = [
    url(r'^testacution', tests.testmesing, name='testmesing'),
    url(r'^testtrac', tests.testtrac, name='testtrac'),
    url(r'^testsinger', tests.testsinger, name='testsinger'),
    url(r'^testUserSimilarity', tests.testUserSimilarity, name='testUserSimilarity'),
    url(r'^testimage', tests.testimage, name='testimage'),
]
