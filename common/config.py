# -*- coding: utf-8 -*-
config = {
    # 0报名， 1任务正在执行，报名已被发布者批准，需要提交数据
    # 2报名被发布者拒绝， 3数据等待审核
    # 4数据需要修改,5数据修改后提交等待审核  6任务完成，数据已经确认
    'TASKJOIN': 0,
    'TASKRUNNING': 1,
    'TASKSTOP': 2,
    'TASKCONFIRM': 3,
    'TASKEDIT': 4,
    'TASKBACK': 5,
    'TASKOVER': 6,
    'LISTNUM': 25,

    'THUMBNAIL_SIZE': (128, 128),

    'task_join': 0,
    'task_running': 1,
    'task_over': 6,
    'task_confirm': 3,
    'task_edit': 4,
    'task_back': 5,
}

hot_task_config = {
    # 查看任务 比重为0.5
    'detail': 0.5,
    # 报名任务，比重为1.5
    'join': 1.5,
}

trade_config = {
    'unpaid': 0,
    'paid': 1,
    'close': 2,
}

trade_pay_config = {
    'sys': 0,
    'alipay': 1,
    'wechat': 2,
}
