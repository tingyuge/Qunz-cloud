# coding=utf-8
from django.core.mail import EmailMultiAlternatives
from django.conf import settings


class SendMessage(object):
    def send_email(self):
        pass

    def send_verify_mail(self, title, message, from_email, to, link):
        html_content = '<a href="http://' + settings.HOST_IP + ':' + settings.HOST_PORT + link + '">http://' + \
                       settings.HOST_IP + ':' + settings.HOST_PORT + link + '</a>'
        self.send_html_email(title, message, html_content, from_email, [to])

    @staticmethod
    def send_html_email(subject, text_content, html_content, from_email, to):
        """
        发送包含html内容的邮件
        :param str subject:
        :param str text_content:
        :param str html_content:
        :param str from_email:
        :param list to:
        :return:
        """
        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
