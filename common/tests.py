from django.test import TestCase
from django.http import HttpResponse

from task.models import Task, UserTask
from people.models import User
from auctionlib import UserSimilarity
# from .common import ImageSelf

import libcode
# Create your tests here.


def testmesing(request):
    libcode.mseningAuction('joinerList', 'task')
    return HttpResponse('ok')


def testtrac(request):
    libcode.tracAuction('joinerList', 'taskList')
    return HttpResponse('ok')


def testsinger(request):
    libcode.singerAuction('joinerList', 'taskList', 100)
    return HttpResponse('ok')


def testUserSimilarity(request):
    usersimilarity = UserSimilarity()
    usersimilarity.userSimilarity('taskList', 'join')
    return HttpResponse('ok')


def testimage(request):
    path = 'image.jpg'
    # image = ImageSelf()
    # image.thumbImage(path)
    return HttpResponse('ok')
