# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import socket
import time
import json
import sys

reload(sys)

from django.http import HttpResponse, JsonResponse

from .models import *
from utils.textprocessing import deal


def task_check(request):
    task = request.GET.get('task')
    id = request.GET.get('id')
    task_obj = Task.objects.get(id=id)
    data = {}
    data['content'] = task_obj.describe
    result = deal.dealSensitiveWord(data['content'])
    if "}" == result:
        result = [["无敏感词"]]
    else:
        bufdic = json.loads(result)
        result = [['*发现敏感词*']]
        for w in bufdic:
            result.append([bufdic[str(w)]["sensitiveWord"], bufdic[str(w)]["start"],bufdic[str(w)]["end"]])

    return JsonResponse({'result': json.dumps(result, ensure_ascii=False)})
