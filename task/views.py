# -*- coding: utf-8 -*-

from django.http import HttpResponse, JsonResponse

from models import *
from services import TaskServices

from common import functions
from app.services import UserLoginDecorator
from utils.code_msg import codeMsg


# Create your views here.


@UserLoginDecorator.login_require
def create_task(request):
    """
    创建任务，同时支付任务酬金 => 平台审核 => 审核通过 => 任务报名 => 选择执行 => 上传数据 => 评价 => 完成
    """
    if request.method != 'POST':
        return JsonResponse({'status': 'error', 'msg': 'error_method'})
    task_base = json.loads(request.POST.get('task_base'))
    task_ext = json.loads(request.POST.get('task_ext'))
    sub_task = json.loads(request.POST.get('sub_task'))
    result = TaskServices.create_task(request.token_store.get('user_id'), task_base, task_ext, sub_task,
                                      request.FILES)
    if result.get('status') is False:
        return JsonResponse({
            'status': False, 'data': '',
            'code': 500, 'message': result.get('msg')
        })
    return JsonResponse({
        'status': True, 'data': result.get('msg'),
        'code': 200, 'message': '',
    })


def update_task(request):
    """
    用户修改任务
    """
    # todo update task
    pass


def get_list(request):
    """
    根据不同的条件，获取任务列表，返回全部的任务
    """
    last_id = int(request.GET.get('last_id', 0))
    # 排序方式 0智能排序，1酬劳最高，2离我最近，3最新上线
    order_type = int(request.GET.get('order_type', 0))
    # 任务类型 0全部任务，1单人任务，2多人任务
    type_task = int(request.GET.get('type_task', 0))
    # 任务数据类型 0全部，1文本，2音频，3图片，4视频，5问卷
    data_type = int(request.GET.get('data_type', 0))
    # 所属行业ID 0全部
    industry = int(request.GET.get('industry', 0))

    task_list = TaskServices.get_task_list(last_id, order_type, type_task, data_type, industry)
    return JsonResponse({
        'status': True,
        'data': task_list,
        'message': len(task_list),
        'code': 200
    })


def get_data_list(request):
    last_id = request.GET.get('last_id', 0)
    data_type = request.GET.get('data_type', 0)
    data_list = TaskServices.get_data_list(int(last_id), int(data_type))
    return JsonResponse({
        'status': True,
        'data': data_list,
        'message': '',
        'code': 200,
    })


def get_check_list(request):
    """
    获取需要审核的任务列表 
    """
    last_id = int(request.GET.get('last_id', 0))
    task_list = TaskServices.get_check_list(last_id)
    return JsonResponse({
        'status': True,
        'data': task_list,
        'message': len(task_list),
        'code': 200,
    })


def pass_data(request):
    """
    数据通过审核
    """
    data_id = int(request.GET.get('data_id', 0))
    task_list = TaskServices.pass_data(data_id)
    return JsonResponse({
        'status': task_list,
        'data': '',
        'message': '',
        'code': 200,
    })


def prevent_data(request):
    """
    数据通过审核
    """
    data_id = int(request.GET.get('data_id', 0))
    task_list = TaskServices.prevent_data(data_id)
    return JsonResponse({
        'status': task_list,
        'data': '',
        'message': '',
        'code': 200,
    })


def pass_check(request):
    """
    任务通过审核
    """
    task_id = request.GET.get('task_id', 0)
    check = TaskServices.pass_check(task_id)
    return JsonResponse({
        'status': check,
        'data': '',
        'message': '',
        'code': 200,
    })


def prevent_check(request):
    """
    任务没有通过审核 
    """
    task_id = request.GET.get('task_id', 0)
    check = TaskServices.prevent_check(task_id)
    return JsonResponse({
        'status': check,
        'data': '',
        'message': '',
        'code': 200,
    })


def task_detail(request):
    """
    获取任务详情
    """
    task_base_id = request.GET.get('id')
    task = TaskServices.get_task(task_base_id)
    return JsonResponse({
        'status': True,
        'data': task,
        'message': '',
        'code': 200,
    })


@UserLoginDecorator.login_require
def task_join_detail(request):
    """
    查看任务详情时，展示用户的任务执行状态 
    """
    user_id = request.token_store.get('user_id', None)
    task_id = request.GET.get('task_id', 0)
    user_join_info = TaskServices.user_join_info(user_id, task_id)
    if user_join_info is None:
        return JsonResponse({
            'status': False,
            'data': '',
            'message': '',
            'code': 200,
        })
    else:
        return JsonResponse({
            'status': True,
            'data': model_to_dict(user_join_info),
            'message': '',
            'code': 200,
        })


@UserLoginDecorator.login_require
def join_task(request):
    """
    用户参加任务
    """
    user_id = request.token_store.get('user_id')
    task_base_id = request.POST.get('id')
    post_price = request.POST.get('post_price', None)
    join = TaskServices.user_join_task(user_id, task_base_id, post_price)
    return JsonResponse({
        'status': join.get('status'),
        'message': join.get('msg'),
        'code': 200 if join.get('status') is True else codeMsg['task_join_error']['code'],
        'data': '',
    })


@UserLoginDecorator.login_require
def get_user_list(request):
    """
    获取用户相关的任务
        正在执行 station = 'task_running'
        已经结束 station = 'task_over'
        已经报名 station = 'task_join'
    """
    user_id = request.token_store.get('user_id')
    last_id = request.GET.get('last_id', 0)
    station = request.GET.get('station', None)
    tasks = TaskServices.user_user_tasks(int(user_id), int(last_id), station)
    return JsonResponse({
        'status': True,
        'data': tasks,
        'message': len(tasks),
        'code': 200,
    })


@UserLoginDecorator.login_require
def get_publish_task(request):
    """
    获取用户上传的任务 
    """
    last_id = request.GET.get('last_id', 0)
    user_id = request.token_store.get('user_id')
    tasks = TaskServices.get_publish_task(int(user_id), int(last_id))
    return JsonResponse({
        'status': True,
        'message': len(tasks),
        'data': tasks,
        'code': 200,
    })


@UserLoginDecorator.login_require
def into_upload_task(request):
    """
    进入提交任务数据界面的渲染
    """
    user_id = request.token_store.get('user_id', 0)
    task_id = request.GET.get('task_id', 0)
    upload_data = TaskServices.into_task_upload(user_id, task_id)
    return JsonResponse({
        'status': True,
        'message': '',
        'data': upload_data,
        'code': 200,
    })


@UserLoginDecorator.login_require
def upload_task_data(request):
    """
    用户上传任务数据
    """
    user_id = request.token_store.get('user_id', 0)
    task_base_id = request.POST.get('task_base_id', 0)
    data_brief = request.POST.get('data_brief', '')
    data_file = request.FILES
    result = TaskServices.upload_task_data(user_id, task_base_id, data_brief, data_file)
    return JsonResponse({
        'status': result.get('status'),
        'message': result.get('msg'),
        'data': '',
        'code': 200,
    })


@UserLoginDecorator.login_require
def update_user_data(request):
    """
    用户修改数据 
    """
    data_id = request.POST.get('data_id', 0)
    user_id = request.token_store.get('user_id', 0)
    task_base_id = request.POST.get('task_base_id', 0)
    data_text = request.POST.get('data_text', '')
    data_brief = request.POST.get('data_brief', '')
    data_file = request.FILES
    result = TaskServices.update_task_data(user_id, data_id, task_base_id, data_text, data_brief, data_file)
    return JsonResponse({
        'status': 'success' if result.get('status') is True else 'error',
        'msg': result.get('msg')
    })


@UserLoginDecorator.login_require
def task_join_user(request):
    """
    获取自己发布的任务的报名用户列表
    """
    user_id = request.token_store.get('user_id', 0)
    task_base_id = request.GET.get('task_base_id', 0)
    join_users = TaskServices.task_join_user(user_id, task_base_id)
    return JsonResponse({
        'status': join_users.get('status'),
        'data': join_users.get('msg'),
        'code': 200,
        'message': '',
    })


@UserLoginDecorator.login_require
def agree_user_join(request):
    """
    发布者同意用户执行任务
    """
    user_id = request.token_store.get('user_id', 0)
    task_base_id = request.POST.get('task_id', 0)
    joiner_ids = json.loads(request.POST.get('joiner_id', '[]'))
    join = TaskServices.agree_user_join(user_id, task_base_id, joiner_ids)
    return JsonResponse({
        'status': join.get('status'),
        'message': join.get('msg'),
        'data': '',
        'code': 200,
    })


@UserLoginDecorator.login_require
def find_task_data(request):
    """
    查看任务数据列表
    """
    user_id = request.token_store.get('user_id')
    task_base_id = request.GET.get('task_id')
    task_data = TaskServices.find_task_data(user_id, task_base_id)
    return JsonResponse({
        'status': task_data.get('status'),
        # 如果msg是[] 会抛出异常
        'data': [] if len(task_data.get('msg')) is 0 else task_data.get('msg'),
        'code': 200,
        'message': '',
    })


@UserLoginDecorator.login_require
def create_comment(request):
    """
    发布者对报名者就行点评 
    """
    user_id = request.token_store.get('user_id')
    to_user = request.POST.get('to_user', 0)
    task = request.POST.get('task_id', 0)
    rank = request.POST.get('rank', 0)
    comment = request.POST.get('comment', '')
    result = TaskServices.create_comment(user_id, to_user, task, rank, comment)
    return JsonResponse({
        'status': 'success' if result.get('status') is True else 'error',
        'msg': result.get('msg')
    })


# todo 获得系统推荐执行任务的用户 自动选择时运行