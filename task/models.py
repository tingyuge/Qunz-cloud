# -*- coding: utf-8 -*-
from django.db import models

from people.models import User
from setting.models import *
from django.forms.models import model_to_dict
from datetime import datetime


# Create your models here.


class TaskExt(models.Model):
    """
    任务属性表，用来记录任务金额等
    """
    # 任务模式(不能为空，1人工选择，2为自动选择)
    task_model = models.IntegerField('任务模式', null=False)
    # 交易模式(不能为空，1为悬赏，2为拍卖)
    trade_model = models.IntegerField('交易模式', null=False)
    # 竞标人数(默认为1人)
    people_num = models.IntegerField('竞标人数', default=1)
    # 预算金额(当交易模式为1时不为空，2时可以为空)悬赏具有
    fare_task = models.FloatField('任务金额', null=True, blank=True)
    # 用户实名要求(默认为0, 1要求， 2不要求)
    real_task = models.IntegerField('实名要求', null=False, default=0)
    # 任务类型(默认为0， 1单人2多人)
    type_task = models.IntegerField('任务类型', null=False, default=0)
    # 任务要求(默认为0, 1全部2部分)
    require_task = models.IntegerField('任务要求', null=False, default=0)
    # 押金(押金必须有) 暂时默认50，后续会根据行业进行动态的修改
    task_fare = models.FloatField('任务押金', null=False, default=50)
    # 交易金额(支付了多少金额),必须有
    trade_fare = models.FloatField('交易金额', null=False, default=0)
    # 招标形式(默认为0, 1荷2英)
    tender_model = models.IntegerField('招标形式', default=0)
    # 任务目标(默认为0,1社会最大，2发布者最大，3带预算最大（当任务金额不为空时出现）)
    aim_task = models.IntegerField('任务目标', null=False, default=0)
    # 审核意见
    opinion = models.CharField('审核意见', null=True, max_length=2000, blank=True)
    # 浏览数
    views = models.IntegerField('浏览数', default=0)
    # 任务提交次数
    submit_num = models.IntegerField('任务提交次数', default=1)
    # 审核员id
    audit_uid = models.IntegerField('审核员id', null=True, blank=True)
    # 所属行业
    industry = models.ForeignKey(Industry, null=True, blank=True)

    @staticmethod
    def find_tasks(*args, **kwargs):
        return TaskExt.objects.filter(*args, **kwargs)


class TaskBase(models.Model):
    """
    具体任务描述等
    """

    class Meta:
        verbose_name = '任务'
        verbose_name_plural = '任务'

    task_ext = models.ForeignKey(TaskExt)
    # 任务编号
    task_num = models.CharField('任务编号', null=True, max_length=50)
    # 任务的发布者
    user = models.ForeignKey(User)
    # 任务名称
    name = models.CharField('任务名称', null=False, max_length=1000)
    # 任务描述
    describe = models.CharField('任务描述', max_length=2000)
    # 任务酬金
    task_value = models.FloatField('任务酬金', null=False)
    # 任务图标
    icon_task = models.FileField('任务图标', upload_to="static/task/%Y%m%d", max_length=255, default="task/demo.jpg")
    # 任务地点
    # 地点类型码,0全国1城市2县区3街道
    location_code = models.IntegerField('地点类型码', default=0, null=False)
    locations = models.CharField('任务地点', max_length=1000)
    # 报名截至时间
    last_time = models.DateTimeField('报名截止时间', null=False)
    # 任务开始时间
    start_time = models.DateTimeField('任务开始时间', null=False)
    # 任务结束时间
    over_time = models.DateTimeField('任务结束时间', null=False)
    # 任务执行上限
    # upper_limit = models.IntegerField(null=False)
    # 数据类型(默认为0,1文本2图片3视频4音频5调查)
    data_type = models.IntegerField('数据类型', null=False)
    # 数据格式 0所有
    # 文本类:1word 2txt 3xml 4excel 5pdf
    # 图片类:1vsd 2opj 3dwg 4bmp 5pcx 6tiff 7gif 8jpeg 9tag 10exif 11fpx 12svg
    #         13psd 14cdr 15pcd 16dxf 17ufo 18eps 19ai 20png 21hdri 22raw
    # 音频类;1cd 2wave 3aiff 4au 5mpeg 6mp3 7mpeg-4 8midi 9wma 10realaudio 11vqf
    #         12oggvorbis 13amr
    # 视频类:1wmv 2asf 3asx 4rm 5rmvb 6mpg 7mpeg 8mpe 9(3gp) 10mov 11mp4 12m4v
    #         13avi 14dat 15mkv 16flv 17vob
    data_format = models.CharField('数据格式', max_length=50, default='')
    # 子任务数
    sub_task_num = models.IntegerField('子任务数', default=0)
    # 父任务ID
    task = models.ForeignKey('self', null=True)
    # 模板位置
    template = models.FileField('模板位置', upload_to="static/task/%Y%m%d", max_length=255, blank=True)
    # 样例位置
    demo = models.FileField('样例位置', upload_to="static/task/%Y%m%d", max_length=255, blank=True)
    # 任务状态(1待审核2待确认3已反馈4报名中5执行中6完成)
    station = models.IntegerField('任务状态', default=1)
    # 最后更新时间
    last_update = models.DateTimeField('最后更新时间', null=False, auto_now=True)
    create_time = models.DateTimeField('创建时间', blank=True, auto_now_add=True)
    update_time = models.DateTimeField('更新时间', blank=True, auto_now=True)

    def __unicode__(self):
        return self.name

    @staticmethod
    def find_tasks(*args, **kwargs):
        return TaskBase.objects.filter(station=4, **kwargs)

    @staticmethod
    def get_task_dict(*args, **kwargs):
        task_base = TaskBase.objects.get(**kwargs)
        print task_base
        task_base = model_to_dict(task_base)
        task_base['icon_task'] = task_base.get('icon_task').name
        task_base['demo'] = task_base.get('demo').name
        task_base['template'] = task_base.get('template').name
        return task_base


class TaskSensor(models.Model):
    """
    任务与传感器对应表
    """
    task = models.ForeignKey(TaskBase)
    sensor = models.ForeignKey(Sensor)


class UserJoinTask(models.Model):
    # todo 任务数据,用户与任务唯一索引
    # 用户
    user = models.ForeignKey(User)
    # 任务
    task_base = models.ForeignKey(TaskBase)
    # 拍卖模式下用户报名时提交的拍卖价格
    post_price = models.FloatField(null=False, default=0)
    # 状态(0报名， 1任务正在执行，报名已被发布者批准，需要提交数据
    # 2报名被发布者拒绝， 3数据等待审核
    # 4数据需要修改,5数据已经反馈，数据再次提交,再次等待审核 6任务完成，数据已经确认)
    station = models.IntegerField(null=False, default=0)
    # 加入时间
    join_time = models.DateTimeField(null=False, auto_now_add=True)
    # 完成任务结束时间，及提交数据时间
    end_time = models.DateTimeField(null=False, auto_now=True)


class UserDataTask(models.Model):
    # 关联的任务
    task_base = models.ForeignKey(TaskBase)
    # 数据提交的用户
    user = models.ForeignKey(User)
    # 数据文本
    data_text = models.TextField('数据文本', null=True)
    # 数据文件
    data_file = models.FileField('数据文件', upload_to='static/task_data/%Y%m%d', max_length=255)
    # 数据简介说明
    data_brief = models.CharField('数据说明', max_length=2048)
    # 数据审核状态
    station = models.IntegerField('审核状态', default=3)
    # 系统提取的数据摘要
    keywords = models.CharField('数据摘要', max_length=2048)
    # 数据最后的更新时间
    update_time = models.DateTimeField('数据最后更新时间', auto_now=True)
    # 数据提交的次数
    submit_num = models.IntegerField('数据提交次数', default=1)
    # 系统评估的数据质量
    quantity = models.FloatField('数据质量', null=True)
    # 数据审核意见与采纳意见
    opinion = models.CharField('数据采纳审核意见', null=True, max_length=2048)
    # 数据审核员
    admin_user = models.IntegerField('数据审核员', null=True)


class UserTaskComment(models.Model):
    # 评论用户
    user = models.ForeignKey(User)
    # 评论指向用户
    to_user = models.ForeignKey(User, related_name='comment_user')
    # 评论附带的任务
    task = models.ForeignKey(TaskBase)
    # 分数
    rank = models.FloatField(default=0)
    # 评论内容
    comment = models.CharField(null=False, max_length=2048)
    # 评论时间
    create_time = models.DateTimeField(auto_now_add=True)



# todo 需要重新拆分表
class Task(models.Model):
    class Meta:
        verbose_name = '任务'
        verbose_name_plural = '任务'

    # 任务编号
    taskid = models.CharField('任务编号', null=True, max_length=50)
    # 发布者id
    ownuser = models.IntegerField('用户ID', null=False)
    # 任务模式(不能为空，1人工选择，2为自动选择)
    taskmodel = models.IntegerField('任务模式', null=False)
    # 交易模式(不能为空，1为悬赏，2为拍卖)
    trademodel = models.IntegerField('交易模式', null=False)
    # 竞标人数(默认为1人)
    peoplenum = models.IntegerField('竞标人数', default=1)
    # 任务金额(当交易模式为1时不为空，2时可以为空)悬赏具有
    faretask = models.FloatField('任务金额', null=True, blank=True)
    # 押金(押金必须有)
    taskfare = models.FloatField('任务押金', null=False, default=0)
    # 交易金额(支付了多少金额),必须有
    tradefare = models.FloatField('交易金额', null=False, default=0)
    # 招标形式(默认为0, 1荷2英)
    tendermodel = models.IntegerField('招标形式', default=0)
    # 任务类型(默认为0， 1单人2多人)
    typetask = models.IntegerField('任务类型', null=False, default=0)
    # 任务要求(默认为0, 1全部2部分)
    requiretask = models.IntegerField('任务要求', null=False, default=0)
    # 任务目标(默认为0,1社会最大，2发布者最大，3带预算最大（当任务金额不为空时出现）)
    aimtask = models.IntegerField('任务目标', null=False, default=0)
    # 用户实名要求(默认为0, 1要求， 2不要求)
    realtask = models.IntegerField('实名要求', null=False, default=0)
    # 任务名称
    name = models.CharField('任务名称', null=False, max_length=1000)
    # 任务描述
    describe = models.CharField('任务描述', max_length=2000)
    # 任务图标
    icontask = models.FileField('任务图标', upload_to="static/task/%Y%m%d", max_length=255, default="task/demo.jpg")
    # 任务地点
    # 地点类型码,0全国1城市2县区3街道
    locacode = models.IntegerField('地点类型码', default=0, null=False)
    locations = models.CharField('任务地点', max_length=1000)
    # 报名截至时间
    lasttime = models.DateTimeField('报名截止时间', null=False)
    # 任务开始时间
    starttime = models.DateTimeField('任务开始时间', null=False)
    # 任务结束时间
    overtime = models.DateTimeField('任务结束时间', null=False)
    # 任务执行上限
    # upperlimit = models.IntegerField(null=False)
    # 数据类型(默认为0,1文本2图片3视频4音频5调查)
    datatype = models.IntegerField('数据类型', null=False)
    # 数据格式 0所有
    # 文本类:1word 2txt 3xml 4excel 5pdf
    # 图片类:1vsd 2opj 3dwg 4bmp 5pcx 6tiff 7gif 8jpeg 9tag 10exif 11fpx 12svg
    #         13psd 14cdr 15pcd 16dxf 17ufo 18eps 19ai 20png 21hdri 22raw
    # 音频类;1cd 2wave 3aiff 4au 5mpeg 6mp3 7mpeg-4 8midi 9wma 10realaudio 11vqf
    #         12oggvorbis 13amr
    # 视频类:1wmv 2asf 3asx 4rm 5rmvb 6mpg 7mpeg 8mpe 9(3gp) 10mov 11mp4 12m4v
    #         13avi 14dat 15mkv 16flv 17vob
    dataformat = models.CharField('数据格式', max_length=50)
    # 子任务数
    subtasknum = models.IntegerField('子任务数', default=0)
    # 模板位置
    templete = models.FileField('模板位置', upload_to="static/task/%Y%m%d", max_length=255, blank=True)
    # 样例位置
    demo = models.FileField('样例位置', upload_to="static/task/%Y%m%d", max_length=255, blank=True)
    # 任务状态(1待审核2待确认3已反馈4报名中5执行中6完成)
    station = models.IntegerField('任务状态', default=1)
    # 审核意见
    opinion = models.CharField('审核意见', null=True, max_length=2000, blank=True)
    # 浏览数
    views = models.IntegerField('浏览数', default=0)
    # 最后更新时间
    lastupdate = models.DateTimeField('最后更新时间', )
    # 任务提交次数
    submitnum = models.IntegerField('任务提交次数', default=1)
    # 审核员id
    audituid = models.IntegerField('审核员id', null=True, blank=True)
    # 所属行业
    industry = models.ForeignKey(Industry, null=True, blank=True)
    # 需要的传感器
    sensor = models.ForeignKey(Sensor, null=True, blank=True)

    def __unicode__(self):
        return self.name

    @classmethod
    def getListById(cls, where, order, lastid):
        getTaskList = Task.objects.filter(stattion=4).values()
        for key, value in where.items():
            if value != 0:
                getTaskList = {
                    'datatype': lambda getTaskList: getTaskList.filter(datatype=value),
                    'typetask': lambda getTaskList: getTaskList.filter(typetask=value),
                    'industry': lambda getTaskList: getTaskList.filter(industry=value),
                }[key](getTaskList)
        # 对任务进行排序
        getTaskList = {
            0: lambda getTaskList: getTaskList.order_by('-id'),
            1: lambda getTaskList: getTaskList.order_by('-faretask'),
            2: lambda getTaskList: getTaskList.order_by('-lastupdate'),
            3: lambda getTaskList: getTaskList.order_by('-lastupdate'),
        }[order](getTaskList)
        getTaskList[lastid: lastid + 25]
        return getTaskList


class SubTask(models.Model):
    class Meta:
        verbose_name = '子任务'
        verbose_name_plural = '子任务'

    # 子任务描述
    subdescribe = models.CharField('子任务描述', max_length=2000)
    # 拍卖情况下子任务的价值
    subprice = models.FloatField('子任务价值', null=False)
    # 子任务的父任务
    parenttask = models.ForeignKey(Task)

    def __unicode__(self):
        return self.subdescribe


class DataTask(models.Model):
    class Meta:
        verbose_name = '任务数据'
        verbose_name_plural = '任务数据'

    # 修改外键
    tid = models.ForeignKey(Task, null=False)
    # 修改外键
    uid = models.ForeignKey(User, null=False)
    # 修改外键
    # 子任务的sid
    sid = models.ForeignKey(SubTask, null=True, blank=True)
    # 数据提交的文本类型
    datatext = models.CharField('数据文本', max_length=2000)
    # 数据位置
    datafile = models.FileField('数据位置', upload_to="static/taskdata/%Y%m%d", max_length=255)
    # 数据说明
    databrief = models.CharField('数据说明', max_length=2000)
    # 数据审核状态(3数据等待审核，4数据需要修改，5数据已经修改，6数据确认)
    station = models.IntegerField('数据审核状态', default=3, null=False)
    # 数据摘要
    keywords = models.CharField('数据摘要', max_length=2000)
    # 数据最后更新时间
    updatetime = models.DateTimeField('数据最后更新时间', null=False)
    # 数据提交次数
    submitnum = models.IntegerField('数据提交次数', default=1)
    # 数据质量
    quality = models.FloatField('数据质量', null=True, blank=True)
    # 数据采纳意见、审核意见
    opinion = models.CharField('数据采纳，审核意见', null=True, blank=True, max_length=2000)
    # 数据审核员
    audituid = models.IntegerField('数据审核员', null=True, blank=True)

    def __unicode__(self):
        return self.databrief


class UserTask(models.Model):
    # 拍卖模式下用户报名时提交的拍卖价格
    postprice = models.FloatField(null=True, blank=True)
    # 状态(# 0报名， 1任务正在执行，报名已被发布者批准，需要提交数据
    # 2报名被发布者拒绝， 3数据等待审核
    # 4数据需要修改,5数据已经反馈，数据再次提交,再次等待审核 6任务完成，数据已经确认)
    station = models.IntegerField(null=False, default=0)
    # 报名时间
    jointime = models.DateTimeField(null=False)
    # 完成时间
    finshtime = models.DateTimeField(null=True, blank=True)
    # 添加外键
    # 如果为单人指向Task,为群体任务指向父任务的Task
    task = models.ForeignKey(Task, null=True, blank=True)
    # 如果为群体的子任务指向SubTask
    subtask = models.ForeignKey(SubTask, null=True, blank=True)
    user = models.ForeignKey(User)
    data = models.ForeignKey(DataTask, null=True, blank=True)

    def __unicode__(self):
        return self.user.nickname

