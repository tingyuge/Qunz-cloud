# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0013_auto_20160320_1843'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='subdescribe',
            field=models.CharField(default=None, max_length=2000),
            preserve_default=False,
        ),
    ]
