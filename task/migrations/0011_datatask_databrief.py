# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0010_auto_20160317_1627'),
    ]

    operations = [
        migrations.AddField(
            model_name='datatask',
            name='databrief',
            field=models.CharField(default=None, max_length=2000),
            preserve_default=False,
        ),
    ]
