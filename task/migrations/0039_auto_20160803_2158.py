# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0038_auto_20160726_2302'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='address',
            new_name='locations',
        ),
        migrations.AddField(
            model_name='task',
            name='locacode',
            field=models.IntegerField(default=0, verbose_name=b'\xe5\x9c\xb0\xe7\x82\xb9\xe7\xb1\xbb\xe5\x9e\x8b\xe7\xa0\x81'),
        ),
    ]
