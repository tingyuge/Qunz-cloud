# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0044_userjointask'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userjointask',
            name='post_price',
            field=models.FloatField(default=0),
        ),
    ]
