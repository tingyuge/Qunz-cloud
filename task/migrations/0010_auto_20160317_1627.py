# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0009_auto_20160316_2129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatask',
            name='station',
            field=models.IntegerField(default=3),
        ),
    ]
