# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0012_auto_20160317_1649'),
    ]

    operations = [
        migrations.AddField(
            model_name='usertask',
            name='data',
            field=models.ForeignKey(to='task.DataTask', null=True),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='datafile',
            field=models.FileField(max_length=255, upload_to=b'taskdata/%Y%m%d'),
        ),
    ]
