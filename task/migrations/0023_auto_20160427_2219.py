# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0022_auto_20160427_2210'),
    ]

    operations = [
        migrations.AddField(
            model_name='usertask',
            name='task',
            field=models.ForeignKey(to='task.Task', null=True),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='sid',
            field=models.ForeignKey(to='task.SubTask', null=True),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='tid',
            field=models.ForeignKey(to='task.Task'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='uid',
            field=models.ForeignKey(to='people.User'),
        ),
        migrations.AlterField(
            model_name='usertask',
            name='tid',
            field=models.IntegerField(),
        ),
    ]
