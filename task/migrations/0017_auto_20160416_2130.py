# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0016_auto_20160405_2137'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='datatask',
            options={'verbose_name': '\u4efb\u52a1\u6570\u636e', 'verbose_name_plural': '\u4efb\u52a1\u6570\u636e'},
        ),
        migrations.AlterModelOptions(
            name='task',
            options={'verbose_name': '\u4efb\u52a1', 'verbose_name_plural': '\u4efb\u52a1'},
        ),
        migrations.AlterField(
            model_name='datatask',
            name='audituid',
            field=models.IntegerField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe5\xae\xa1\xe6\xa0\xb8\xe5\x91\x98'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='databrief',
            field=models.CharField(max_length=2000, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe8\xaf\xb4\xe6\x98\x8e'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='datafile',
            field=models.FileField(upload_to=b'taskdata/%Y%m%d', max_length=255, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe4\xbd\x8d\xe7\xbd\xae'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='keywords',
            field=models.CharField(max_length=2000, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x91\x98\xe8\xa6\x81'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='opinion',
            field=models.FloatField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe9\x87\x87\xe7\xba\xb3\xef\xbc\x8c\xe5\xae\xa1\xe6\xa0\xb8\xe6\x84\x8f\xe8\xa7\x81'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='quality',
            field=models.FloatField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe8\xb4\xa8\xe9\x87\x8f'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='station',
            field=models.IntegerField(default=3, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe5\xae\xa1\xe6\xa0\xb8\xe7\x8a\xb6\xe6\x80\x81'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='submitnum',
            field=models.IntegerField(default=1, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x8f\x90\xe4\xba\xa4\xe6\xac\xa1\xe6\x95\xb0'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='updatetime',
            field=models.DateTimeField(verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x9c\x80\xe5\x90\x8e\xe6\x9b\xb4\xe6\x96\xb0\xe6\x97\xb6\xe9\x97\xb4'),
        ),
        migrations.AlterField(
            model_name='subtask',
            name='subdescribe',
            field=models.CharField(max_length=2000, verbose_name=b'\xe5\xad\x90\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x8f\x8f\xe8\xbf\xb0'),
        ),
        migrations.AlterField(
            model_name='subtask',
            name='subprice',
            field=models.FloatField(verbose_name=b'\xe5\xad\x90\xe4\xbb\xbb\xe5\x8a\xa1\xe4\xbb\xb7\xe5\x80\xbc'),
        ),
        migrations.AlterField(
            model_name='task',
            name='address',
            field=models.CharField(max_length=1000, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\x9c\xb0\xe7\x82\xb9'),
        ),
        migrations.AlterField(
            model_name='task',
            name='aimtask',
            field=models.IntegerField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\x9b\xae\xe6\xa0\x87'),
        ),
        migrations.AlterField(
            model_name='task',
            name='audituid',
            field=models.IntegerField(null=True, verbose_name=b'\xe5\xae\xa1\xe6\xa0\xb8\xe5\x91\x98id'),
        ),
        migrations.AlterField(
            model_name='task',
            name='dataformat',
            field=models.CharField(max_length=50, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\xa0\xbc\xe5\xbc\x8f'),
        ),
        migrations.AlterField(
            model_name='task',
            name='datatype',
            field=models.IntegerField(verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe7\xb1\xbb\xe5\x9e\x8b'),
        ),
        migrations.AlterField(
            model_name='task',
            name='demo',
            field=models.FileField(upload_to=b'task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa0\xb7\xe4\xbe\x8b\xe4\xbd\x8d\xe7\xbd\xae'),
        ),
        migrations.AlterField(
            model_name='task',
            name='describe',
            field=models.CharField(max_length=2000, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x8f\x8f\xe8\xbf\xb0'),
        ),
        migrations.AlterField(
            model_name='task',
            name='faretask',
            field=models.FloatField(null=True, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe9\x87\x91\xe9\xa2\x9d'),
        ),
        migrations.AlterField(
            model_name='task',
            name='icontask',
            field=models.FileField(default=b'task/demo.jpg', upload_to=b'task/%Y%m%d', max_length=255, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\x9b\xbe\xe6\xa0\x87'),
        ),
        migrations.AlterField(
            model_name='task',
            name='industry',
            field=models.CharField(max_length=200, null=True, verbose_name=b'\xe6\x89\x80\xe5\xb1\x9e\xe8\xa1\x8c\xe4\xb8\x9a'),
        ),
        migrations.AlterField(
            model_name='task',
            name='lasttime',
            field=models.DateTimeField(verbose_name=b'\xe6\x8a\xa5\xe5\x90\x8d\xe6\x88\xaa\xe6\xad\xa2\xe6\x97\xb6\xe9\x97\xb4'),
        ),
        migrations.AlterField(
            model_name='task',
            name='lastupdate',
            field=models.DateTimeField(verbose_name=b'\xe6\x9c\x80\xe5\x90\x8e\xe6\x9b\xb4\xe6\x96\xb0\xe6\x97\xb6\xe9\x97\xb4'),
        ),
        migrations.AlterField(
            model_name='task',
            name='name',
            field=models.CharField(max_length=1000, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\x90\x8d\xe7\xa7\xb0'),
        ),
        migrations.AlterField(
            model_name='task',
            name='overtime',
            field=models.DateTimeField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\xbb\x93\xe6\x9d\x9f\xe6\x97\xb6\xe9\x97\xb4'),
        ),
        migrations.AlterField(
            model_name='task',
            name='peoplenum',
            field=models.IntegerField(default=1, verbose_name=b'\xe7\xab\x9e\xe6\xa0\x87\xe4\xba\xba\xe6\x95\xb0'),
        ),
        migrations.AlterField(
            model_name='task',
            name='realtask',
            field=models.IntegerField(default=0, verbose_name=b'\xe5\xae\x9e\xe5\x90\x8d\xe8\xa6\x81\xe6\xb1\x82'),
        ),
        migrations.AlterField(
            model_name='task',
            name='requiretask',
            field=models.IntegerField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe8\xa6\x81\xe6\xb1\x82'),
        ),
        migrations.AlterField(
            model_name='task',
            name='starttime',
            field=models.DateTimeField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\xbc\x80\xe5\xa7\x8b\xe6\x97\xb6\xe9\x97\xb4'),
        ),
        migrations.AlterField(
            model_name='task',
            name='station',
            field=models.IntegerField(default=1, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\x8a\xb6\xe6\x80\x81'),
        ),
        migrations.AlterField(
            model_name='task',
            name='submitnum',
            field=models.IntegerField(default=1, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x8f\x90\xe4\xba\xa4\xe6\xac\xa1\xe6\x95\xb0'),
        ),
        migrations.AlterField(
            model_name='task',
            name='subtasknum',
            field=models.IntegerField(default=0, verbose_name=b'\xe5\xad\x90\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x95\xb0'),
        ),
        migrations.AlterField(
            model_name='task',
            name='taskid',
            field=models.CharField(max_length=50, null=True, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\xbc\x96\xe5\x8f\xb7'),
        ),
        migrations.AlterField(
            model_name='task',
            name='taskmodel',
            field=models.IntegerField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\xa8\xa1\xe5\xbc\x8f'),
        ),
        migrations.AlterField(
            model_name='task',
            name='templete',
            field=models.FileField(upload_to=b'task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa8\xa1\xe6\x9d\xbf\xe4\xbd\x8d\xe7\xbd\xae'),
        ),
        migrations.AlterField(
            model_name='task',
            name='tendermodel',
            field=models.IntegerField(default=0, verbose_name=b'\xe6\x8b\x9b\xe6\xa0\x87\xe5\xbd\xa2\xe5\xbc\x8f'),
        ),
        migrations.AlterField(
            model_name='task',
            name='trademodel',
            field=models.IntegerField(verbose_name=b'\xe4\xba\xa4\xe6\x98\x93\xe6\xa8\xa1\xe5\xbc\x8f'),
        ),
        migrations.AlterField(
            model_name='task',
            name='typetask',
            field=models.IntegerField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\xb1\xbb\xe5\x9e\x8b'),
        ),
        migrations.AlterField(
            model_name='task',
            name='views',
            field=models.IntegerField(default=0, verbose_name=b'\xe6\xb5\x8f\xe8\xa7\x88\xe6\x95\xb0'),
        ),
    ]
