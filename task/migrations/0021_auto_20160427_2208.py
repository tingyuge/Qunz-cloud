# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0020_auto_20160427_2057'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='taskfare',
            field=models.FloatField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x8a\xbc\xe9\x87\x91'),
        ),
        migrations.AddField(
            model_name='task',
            name='tradefare',
            field=models.FloatField(default=0, verbose_name=b'\xe4\xba\xa4\xe6\x98\x93\xe9\x87\x91\xe9\xa2\x9d'),
        ),
    ]
