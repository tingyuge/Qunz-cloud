# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0046_auto_20170428_1907'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdatatask',
            name='admin_user',
            field=models.IntegerField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe5\xae\xa1\xe6\xa0\xb8\xe5\x91\x98'),
        ),
    ]
