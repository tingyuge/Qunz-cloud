# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0048_auto_20170428_2117'),
    ]

    operations = [
        migrations.RenameField(
            model_name='taskbase',
            old_name='overtime',
            new_name='over_time',
        ),
        migrations.AlterField(
            model_name='taskbase',
            name='data_format',
            field=models.CharField(default=b'', max_length=50, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\xa0\xbc\xe5\xbc\x8f'),
        ),
    ]
