# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0039_auto_20160803_2158'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usertask',
            name='sid',
        ),
        migrations.RemoveField(
            model_name='usertask',
            name='tid',
        ),
        migrations.RemoveField(
            model_name='usertask',
            name='uid',
        ),
    ]
