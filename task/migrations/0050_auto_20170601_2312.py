# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0049_auto_20170528_2133'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskbase',
            name='create_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 1, 15, 11, 53, 939115, tzinfo=utc), verbose_name=b'\xe5\x88\x9b\xe5\xbb\xba\xe6\x97\xb6\xe9\x97\xb4', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskbase',
            name='update_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 1, 15, 12, 4, 884462, tzinfo=utc), verbose_name=b'\xe6\x9b\xb4\xe6\x96\xb0\xe6\x97\xb6\xe9\x97\xb4', auto_now=True),
            preserve_default=False,
        ),
    ]
