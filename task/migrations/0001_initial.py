# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parenttask', models.IntegerField(null=True)),
                ('ownuser', models.IntegerField()),
                ('taskmodel', models.IntegerField()),
                ('tradeModel', models.IntegerField()),
                ('peoplenum', models.IntegerField(default=1)),
                ('faretask', models.FloatField(null=True)),
                ('tendermodel', models.IntegerField(default=0)),
                ('typetask', models.IntegerField(default=0)),
                ('requiretask', models.IntegerField(default=0)),
                ('aimtask', models.IntegerField(default=0)),
                ('realtask', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=1000)),
                ('describe', models.CharField(max_length=2000)),
                ('address', models.CharField(max_length=1000)),
                ('lasttime', models.TimeField()),
                ('starttime', models.TimeField()),
                ('overtime', models.TimeField()),
                ('upperlimit', models.IntegerField()),
                ('dataformat', models.CharField(max_length=50)),
                ('subtask', models.IntegerField(default=0)),
                ('templete', models.CharField(max_length=255)),
                ('demo', models.CharField(max_length=255)),
                ('station', models.IntegerField(default=1)),
                ('views', models.IntegerField(default=0)),
                ('lastupdate', models.TimeField()),
                ('submitnum', models.IntegerField(default=1)),
                ('audituid', models.IntegerField(null=True)),
            ],
        ),
    ]
