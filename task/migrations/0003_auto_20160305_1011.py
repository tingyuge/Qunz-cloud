# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0002_auto_20160301_2229'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='icontask',
            field=models.FileField(default=None, max_length=255, upload_to=b'task/%Y%m%d'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='industry',
            field=models.CharField(default=None, max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='task',
            name='demo',
            field=models.FileField(max_length=255, upload_to=b'task/%Y%m%d'),
        ),
        migrations.AlterField(
            model_name='task',
            name='templete',
            field=models.FileField(max_length=255, upload_to=b'task/%Y%m%d'),
        ),
    ]
