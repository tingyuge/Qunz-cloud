# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0010_auto_20170416_1125'),
        ('task', '0045_auto_20170421_0138'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserDataTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_text', models.TextField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x96\x87\xe6\x9c\xac')),
                ('data_file', models.FileField(upload_to=b'static/task_data/%Y%m%d', max_length=255, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x96\x87\xe4\xbb\xb6')),
                ('data_brief', models.CharField(max_length=2048, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe8\xaf\xb4\xe6\x98\x8e')),
                ('station', models.IntegerField(default=3, verbose_name=b'\xe5\xae\xa1\xe6\xa0\xb8\xe7\x8a\xb6\xe6\x80\x81')),
                ('keywords', models.CharField(max_length=2048, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x91\x98\xe8\xa6\x81')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x9c\x80\xe5\x90\x8e\xe6\x9b\xb4\xe6\x96\xb0\xe6\x97\xb6\xe9\x97\xb4')),
                ('submit_num', models.IntegerField(default=1, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x8f\x90\xe4\xba\xa4\xe6\xac\xa1\xe6\x95\xb0')),
                ('quantity', models.FloatField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe8\xb4\xa8\xe9\x87\x8f')),
                ('opinion', models.CharField(max_length=2048, null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe9\x87\x87\xe7\xba\xb3\xe5\xae\xa1\xe6\xa0\xb8\xe6\x84\x8f\xe8\xa7\x81')),
                ('admin_user', models.IntegerField(verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe5\xae\xa1\xe6\xa0\xb8\xe5\x91\x98')),
                ('task_base', models.ForeignKey(to='task.TaskBase')),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
        migrations.CreateModel(
            name='UserTaskComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_time', models.DateTimeField(auto_created=True)),
                ('rank', models.FloatField(default=0)),
                ('comment', models.CharField(max_length=2048)),
                ('task', models.ForeignKey(to='task.TaskBase')),
                ('to_user', models.ForeignKey(related_name='comment_user', to='people.User')),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
        migrations.RemoveField(
            model_name='usercomment',
            name='subtask',
        ),
        migrations.RemoveField(
            model_name='usercomment',
            name='task',
        ),
        migrations.RemoveField(
            model_name='usercomment',
            name='user',
        ),
        migrations.DeleteModel(
            name='UserComment',
        ),
    ]
