# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0003_auto_20160305_1011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='industry',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='lasttime',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='task',
            name='lastupdate',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='task',
            name='overtime',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='task',
            name='starttime',
            field=models.DateTimeField(),
        ),
    ]
