# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('setting', '0006_sensor'),
        ('task', '0036_task_industry'),
    ]

    operations = [
        migrations.AddField(
            model_name='datatask',
            name='datatext',
            field=models.CharField(default=None, max_length=2000, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\x96\x87\xe6\x9c\xac'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='sensor',
            field=models.ForeignKey(blank=True, to='setting.Sensor', null=True),
        ),
    ]
