# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0042_auto_20170419_1640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskbase',
            name='task_value',
            field=models.FloatField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe9\x85\xac\xe9\x87\x91'),
        ),
    ]
