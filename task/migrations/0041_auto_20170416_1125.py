# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('setting', '0007_versions'),
        ('people', '0010_auto_20170416_1125'),
        ('task', '0040_auto_20160816_2202'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_id', models.CharField(max_length=50, null=True, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\xbc\x96\xe5\x8f\xb7')),
                ('name', models.CharField(max_length=1000, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\x90\x8d\xe7\xa7\xb0')),
                ('describe', models.CharField(max_length=2000, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x8f\x8f\xe8\xbf\xb0')),
                ('task_value', models.FloatField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe4\xbb\xb7\xe5\x80\xbc')),
                ('icon_task', models.FileField(default=b'task/demo.jpg', upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\x9b\xbe\xe6\xa0\x87')),
                ('location_code', models.IntegerField(default=0, verbose_name=b'\xe5\x9c\xb0\xe7\x82\xb9\xe7\xb1\xbb\xe5\x9e\x8b\xe7\xa0\x81')),
                ('locations', models.CharField(max_length=1000, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\x9c\xb0\xe7\x82\xb9')),
                ('last_time', models.DateTimeField(verbose_name=b'\xe6\x8a\xa5\xe5\x90\x8d\xe6\x88\xaa\xe6\xad\xa2\xe6\x97\xb6\xe9\x97\xb4')),
                ('start_time', models.DateTimeField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\xbc\x80\xe5\xa7\x8b\xe6\x97\xb6\xe9\x97\xb4')),
                ('overtime', models.DateTimeField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\xbb\x93\xe6\x9d\x9f\xe6\x97\xb6\xe9\x97\xb4')),
                ('data_type', models.IntegerField(verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe7\xb1\xbb\xe5\x9e\x8b')),
                ('data_format', models.CharField(max_length=50, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe6\xa0\xbc\xe5\xbc\x8f')),
                ('sub_task_num', models.IntegerField(default=0, verbose_name=b'\xe5\xad\x90\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x95\xb0')),
                ('template', models.FileField(upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa8\xa1\xe6\x9d\xbf\xe4\xbd\x8d\xe7\xbd\xae', blank=True)),
                ('demo', models.FileField(upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa0\xb7\xe4\xbe\x8b\xe4\xbd\x8d\xe7\xbd\xae', blank=True)),
                ('station', models.IntegerField(default=1, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\x8a\xb6\xe6\x80\x81')),
                ('last_update', models.DateTimeField(verbose_name=b'\xe6\x9c\x80\xe5\x90\x8e\xe6\x9b\xb4\xe6\x96\xb0\xe6\x97\xb6\xe9\x97\xb4')),
            ],
            options={
                'verbose_name': '\u4efb\u52a1',
                'verbose_name_plural': '\u4efb\u52a1',
            },
        ),
        migrations.CreateModel(
            name='TaskExt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_model', models.IntegerField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\xa8\xa1\xe5\xbc\x8f')),
                ('trade_model', models.IntegerField(verbose_name=b'\xe4\xba\xa4\xe6\x98\x93\xe6\xa8\xa1\xe5\xbc\x8f')),
                ('people_num', models.IntegerField(default=1, verbose_name=b'\xe7\xab\x9e\xe6\xa0\x87\xe4\xba\xba\xe6\x95\xb0')),
                ('fare_task', models.FloatField(null=True, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe9\x87\x91\xe9\xa2\x9d', blank=True)),
                ('real_task', models.IntegerField(default=0, verbose_name=b'\xe5\xae\x9e\xe5\x90\x8d\xe8\xa6\x81\xe6\xb1\x82')),
                ('type_task', models.IntegerField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\xb1\xbb\xe5\x9e\x8b')),
                ('require_task', models.IntegerField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe8\xa6\x81\xe6\xb1\x82')),
                ('task_fare', models.FloatField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x8a\xbc\xe9\x87\x91')),
                ('trade_fare', models.FloatField(default=0, verbose_name=b'\xe4\xba\xa4\xe6\x98\x93\xe9\x87\x91\xe9\xa2\x9d')),
                ('tender_model', models.IntegerField(default=0, verbose_name=b'\xe6\x8b\x9b\xe6\xa0\x87\xe5\xbd\xa2\xe5\xbc\x8f')),
                ('aim_task', models.IntegerField(default=0, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe7\x9b\xae\xe6\xa0\x87')),
                ('opinion', models.CharField(max_length=2000, null=True, verbose_name=b'\xe5\xae\xa1\xe6\xa0\xb8\xe6\x84\x8f\xe8\xa7\x81', blank=True)),
                ('views', models.IntegerField(default=0, verbose_name=b'\xe6\xb5\x8f\xe8\xa7\x88\xe6\x95\xb0')),
                ('submit_num', models.IntegerField(default=1, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe6\x8f\x90\xe4\xba\xa4\xe6\xac\xa1\xe6\x95\xb0')),
                ('audit_uid', models.IntegerField(null=True, verbose_name=b'\xe5\xae\xa1\xe6\xa0\xb8\xe5\x91\x98id', blank=True)),
                ('industry', models.ForeignKey(blank=True, to='setting.Industry', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TaskSensor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sensor', models.ForeignKey(to='setting.Sensor')),
                ('task', models.ForeignKey(to='task.TaskBase')),
            ],
        ),
        migrations.AddField(
            model_name='taskbase',
            name='task_ext',
            field=models.ForeignKey(to='task.TaskExt'),
        ),
        migrations.AddField(
            model_name='taskbase',
            name='user',
            field=models.ForeignKey(to='people.User'),
        ),
    ]
