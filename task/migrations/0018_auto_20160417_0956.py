# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0017_auto_20160416_2130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatask',
            name='datafile',
            field=models.FileField(upload_to=b'static/taskdata/%Y%m%d', max_length=255, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe4\xbd\x8d\xe7\xbd\xae'),
        ),
        migrations.AlterField(
            model_name='task',
            name='demo',
            field=models.FileField(upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa0\xb7\xe4\xbe\x8b\xe4\xbd\x8d\xe7\xbd\xae'),
        ),
        migrations.AlterField(
            model_name='task',
            name='icontask',
            field=models.FileField(default=b'task/demo.jpg', upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe5\x9b\xbe\xe6\xa0\x87'),
        ),
        migrations.AlterField(
            model_name='task',
            name='templete',
            field=models.FileField(upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa8\xa1\xe6\x9d\xbf\xe4\xbd\x8d\xe7\xbd\xae'),
        ),
    ]
