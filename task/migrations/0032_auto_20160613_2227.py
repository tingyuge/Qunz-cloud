# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0031_auto_20160527_1312'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usercomment',
            old_name='score',
            new_name='rank',
        ),
        migrations.AddField(
            model_name='usercomment',
            name='subtask',
            field=models.ForeignKey(default=1, to='task.SubTask'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='datatask',
            name='audituid',
            field=models.IntegerField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe5\xae\xa1\xe6\xa0\xb8\xe5\x91\x98', blank=True),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='opinion',
            field=models.CharField(max_length=2000, null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe9\x87\x87\xe7\xba\xb3\xef\xbc\x8c\xe5\xae\xa1\xe6\xa0\xb8\xe6\x84\x8f\xe8\xa7\x81', blank=True),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='quality',
            field=models.FloatField(null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe8\xb4\xa8\xe9\x87\x8f', blank=True),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='sid',
            field=models.ForeignKey(blank=True, to='task.SubTask', null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='audituid',
            field=models.IntegerField(null=True, verbose_name=b'\xe5\xae\xa1\xe6\xa0\xb8\xe5\x91\x98id', blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='demo',
            field=models.FileField(upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa0\xb7\xe4\xbe\x8b\xe4\xbd\x8d\xe7\xbd\xae', blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='faretask',
            field=models.FloatField(null=True, verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1\xe9\x87\x91\xe9\xa2\x9d', blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='industry',
            field=models.ForeignKey(blank=True, to='setting.Industry', null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='opinion',
            field=models.CharField(max_length=2000, null=True, verbose_name=b'\xe5\xae\xa1\xe6\xa0\xb8\xe6\x84\x8f\xe8\xa7\x81', blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='ownuser',
            field=models.IntegerField(verbose_name=b'\xe7\x94\xa8\xe6\x88\xb7ID'),
        ),
        migrations.AlterField(
            model_name='task',
            name='templete',
            field=models.FileField(upload_to=b'static/task/%Y%m%d', max_length=255, verbose_name=b'\xe6\xa8\xa1\xe6\x9d\xbf\xe4\xbd\x8d\xe7\xbd\xae', blank=True),
        ),
        migrations.AlterField(
            model_name='usercomment',
            name='task',
            field=models.ForeignKey(to='task.Task'),
        ),
        migrations.AlterField(
            model_name='usertask',
            name='data',
            field=models.ForeignKey(blank=True, to='task.DataTask', null=True),
        ),
        migrations.AlterField(
            model_name='usertask',
            name='finshtime',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='usertask',
            name='postprice',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='usertask',
            name='sid',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='usertask',
            name='subtask',
            field=models.ForeignKey(blank=True, to='task.SubTask', null=True),
        ),
        migrations.AlterField(
            model_name='usertask',
            name='task',
            field=models.ForeignKey(blank=True, to='task.Task', null=True),
        ),
    ]
