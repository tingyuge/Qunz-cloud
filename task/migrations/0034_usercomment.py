# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0008_auto_20160613_2223'),
        ('task', '0033_auto_20160621_1035'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('touser', models.IntegerField()),
                ('rank', models.FloatField(default=0)),
                ('comment', models.CharField(max_length=500)),
                ('posttime', models.DateTimeField()),
                ('subtask', models.ForeignKey(to='task.SubTask')),
                ('task', models.ForeignKey(to='task.Task')),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
    ]
