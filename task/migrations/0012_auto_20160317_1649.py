# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0011_datatask_databrief'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatask',
            name='opinion',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='quality',
            field=models.FloatField(null=True),
        ),
    ]
