# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0010_auto_20170416_1125'),
        ('task', '0043_auto_20170419_1717'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserJoinTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_price', models.FloatField(null=True, blank=True)),
                ('station', models.IntegerField(default=0)),
                ('join_time', models.DateTimeField(auto_now_add=True)),
                ('end_time', models.DateTimeField(auto_now=True)),
                ('task_base', models.ForeignKey(to='task.TaskBase')),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
    ]
