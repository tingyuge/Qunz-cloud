# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0015_auto_20160404_2056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usertask',
            name='postprice',
            field=models.FloatField(null=True),
        ),
    ]
