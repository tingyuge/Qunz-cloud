# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0021_auto_20160427_2208'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usertask',
            name='task',
        ),
        migrations.AlterField(
            model_name='usertask',
            name='tid',
            field=models.ForeignKey(to='task.Task'),
        ),
    ]
