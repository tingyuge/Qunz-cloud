# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0004_auto_20160307_2108'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField()),
                ('tid', models.IntegerField()),
                ('station', models.IntegerField(default=0)),
                ('jointime', models.DateTimeField()),
                ('finshtime', models.DateTimeField()),
            ],
        ),
    ]
