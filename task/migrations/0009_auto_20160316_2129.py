# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0008_auto_20160315_2117'),
    ]

    operations = [
        migrations.CreateModel(
            name='DataTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tid', models.IntegerField()),
                ('uid', models.IntegerField()),
                ('datafile', models.FileField(max_length=255, upload_to=b'task/%Y%m%d')),
                ('station', models.IntegerField(default=1)),
                ('keywords', models.CharField(max_length=2000)),
                ('updatetime', models.DateTimeField()),
                ('submitnum', models.IntegerField(default=1)),
                ('quality', models.FloatField()),
                ('opinion', models.FloatField()),
                ('audituid', models.IntegerField(null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='task',
            name='icontask',
            field=models.FileField(default=b'task/demo.jpg', max_length=255, upload_to=b'task/%Y%m%d'),
        ),
    ]
