# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0005_usertask'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usertask',
            name='finshtime',
            field=models.DateTimeField(null=True),
        ),
    ]
