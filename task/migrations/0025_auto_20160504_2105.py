# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0024_usercomment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='industry',
            field=models.ForeignKey(to='setting.Industry', null=True),
        ),
    ]
