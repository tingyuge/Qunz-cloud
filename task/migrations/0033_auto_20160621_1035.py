# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0032_auto_20160613_2227'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usercomment',
            name='subtask',
        ),
        migrations.RemoveField(
            model_name='usercomment',
            name='task',
        ),
        migrations.RemoveField(
            model_name='usercomment',
            name='user',
        ),
        migrations.DeleteModel(
            name='UserComment',
        ),
    ]
