# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0041_auto_20170416_1125'),
    ]

    operations = [
        migrations.RenameField(
            model_name='taskbase',
            old_name='task_id',
            new_name='task_num',
        ),
        migrations.AddField(
            model_name='taskbase',
            name='task',
            field=models.ForeignKey(to='task.TaskBase', null=True),
        ),
        migrations.AlterField(
            model_name='taskbase',
            name='last_update',
            field=models.DateTimeField(auto_now=True, verbose_name=b'\xe6\x9c\x80\xe5\x90\x8e\xe6\x9b\xb4\xe6\x96\xb0\xe6\x97\xb6\xe9\x97\xb4'),
        ),
    ]
