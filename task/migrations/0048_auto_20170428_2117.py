# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0047_auto_20170428_1909'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usertaskcomment',
            name='create_time',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
