# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0014_task_subdescribe'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subdescribe', models.CharField(max_length=2000)),
                ('subprice', models.FloatField()),
            ],
        ),
        migrations.RenameField(
            model_name='task',
            old_name='upperlimit',
            new_name='datatype',
        ),
        migrations.RenameField(
            model_name='task',
            old_name='subtask',
            new_name='subtasknum',
        ),
        migrations.RemoveField(
            model_name='task',
            name='parenttask',
        ),
        migrations.RemoveField(
            model_name='task',
            name='subdescribe',
        ),
        migrations.AddField(
            model_name='datatask',
            name='sid',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='usertask',
            name='postprice',
            field=models.FloatField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='usertask',
            name='sid',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='subtask',
            name='parenttask',
            field=models.ForeignKey(to='task.Task'),
        ),
        migrations.AddField(
            model_name='usertask',
            name='subtask',
            field=models.ForeignKey(to='task.SubTask', null=True),
        ),
    ]
