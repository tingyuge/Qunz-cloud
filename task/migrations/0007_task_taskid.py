# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0006_auto_20160312_1956'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='taskid',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
