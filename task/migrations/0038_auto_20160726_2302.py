# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0037_auto_20160622_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usercomment',
            name='subtask',
            field=models.ForeignKey(to='task.SubTask', null=True),
        ),
    ]
