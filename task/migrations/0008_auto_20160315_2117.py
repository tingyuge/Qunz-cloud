# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('task', '0007_task_taskid'),
    ]

    operations = [
        migrations.AddField(
            model_name='usertask',
            name='task',
            field=models.ForeignKey(default=None, to='task.Task'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='usertask',
            name='user',
            field=models.ForeignKey(default=None, to='people.User'),
            preserve_default=False,
        ),
    ]
