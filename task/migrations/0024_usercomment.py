# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0005_user_station'),
        ('task', '0023_auto_20160427_2219'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('touser', models.IntegerField()),
                ('score', models.FloatField(default=0)),
                ('comment', models.CharField(max_length=500)),
                ('posttime', models.DateTimeField()),
                ('task', models.ForeignKey(to='task.Task', null=True)),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
    ]
