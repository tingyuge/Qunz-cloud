# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0019_auto_20160417_1508'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subtask',
            options={'verbose_name': '\u5b50\u4efb\u52a1', 'verbose_name_plural': '\u5b50\u4efb\u52a1'},
        ),
    ]
