# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0025_auto_20160504_2105'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='industry',
        ),
    ]
