# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0018_auto_20160417_0956'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='opinion',
            field=models.CharField(max_length=2000, null=True, verbose_name=b'\xe5\xae\xa1\xe6\xa0\xb8\xe6\x84\x8f\xe8\xa7\x81'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='opinion',
            field=models.CharField(max_length=2000, null=True, verbose_name=b'\xe6\x95\xb0\xe6\x8d\xae\xe9\x87\x87\xe7\xba\xb3\xef\xbc\x8c\xe5\xae\xa1\xe6\xa0\xb8\xe6\x84\x8f\xe8\xa7\x81'),
        ),
        migrations.AlterField(
            model_name='datatask',
            name='tid',
            field=models.IntegerField(verbose_name=b'\xe4\xbb\xbb\xe5\x8a\xa1id'),
        ),
    ]
