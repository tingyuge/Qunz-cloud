# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('setting', '0005_industry'),
        ('task', '0035_remove_task_industry'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='industry',
            field=models.ForeignKey(blank=True, to='setting.Industry', null=True),
        ),
    ]
