# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0029_remove_task_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='industry',
            field=models.ForeignKey(default=None, to='setting.Industry'),
            preserve_default=False,
        ),
    ]
