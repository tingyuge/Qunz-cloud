# -*- coding: utf-8 -*-
import datetime
import time
import pytz

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.utils import ValidationError

from .models import *

from people.models import User
from setting.models import Industry


class TaskBaseForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all())
    name = forms.CharField(required=True)
    describe = forms.CharField(required=True)
    icon_task = forms.FileField(required=False)
    last_time = forms.DateTimeField(required=True)
    start_time = forms.DateTimeField(required=True)
    over_time = forms.DateTimeField(required=True)
    data_type = forms.IntegerField(required=True)
    data_format = forms.IntegerField(required=False)
    sub_task_num = forms.IntegerField(required=True)
    template = forms.FileField(required=False)
    demo = forms.FileField(required=False)
    task_value = forms.FloatField(required=True)
    task_ext = forms.ModelChoiceField(queryset=TaskExt.objects.all())
    task = forms.ModelChoiceField(queryset=TaskBase.objects.all(), required=False)
    task_num = forms.CharField(required=True)
    create_time = forms.DateTimeField(required=False)

    class Meta:
        fields = [
            'user', 'name', 'describe', 'icon_task', 'task_ext',
            'last_time', 'start_time', "over_time", 'data_type',
            'data_format', 'sub_task_num', 'template', 'demo', 'task_value',
            'task', 'task_num', 'create_time'
        ]
        model = TaskBase


class TaskExtForm(forms.ModelForm):
    task_model = forms.IntegerField(required=True)
    trade_model = forms.IntegerField(required=True)
    people_num = forms.IntegerField(required=True)
    fare_task = forms.FloatField(required=True)
    real_task = forms.IntegerField(required=True)
    type_task = forms.IntegerField(required=True)
    require_task = forms.IntegerField(required=True)
    task_fare = forms.FloatField(required=True)
    trade_fare = forms.FloatField(required=True)
    tender_model = forms.IntegerField(required=True)
    aim_task = forms.IntegerField(required=True)
    # industry = forms.ModelChoiceField(queryset=Industry.objects.all())

    class Meta:
        fields = [
            'task_model', 'trade_model', 'people_num', 'fare_task', 'real_task',
            'type_task', 'require_task', 'task_fare', 'trade_fare', 'tender_model', 'aim_task'
        ]
        model = TaskExt


class TaskSensorForm(forms.ModelForm):
    task = forms.ModelChoiceField(queryset=TaskBase.objects.all())
    sensor = forms.ModelChoiceField(queryset=Sensor.objects.all())

    class Meta:
        fields = ['task', 'sensor']
        model = TaskSensor


class UserJoinForm(forms.ModelForm):
    user = forms.ModelChoiceField(required=False, queryset=User.objects.all())
    post_price = forms.FloatField(required=False)
    station = forms.IntegerField(required=True)
    task_base = forms.ModelChoiceField(required=True, queryset=TaskBase.objects.all())

    class Meta:
        fields = ['post_price', 'station', 'task_base', 'user']
        model = UserJoinTask


class UserDataForm(forms.ModelForm):
    user = forms.ModelChoiceField(required=True, queryset=User.objects.all())
    task_base = forms.ModelChoiceField(required=True, queryset=TaskBase.objects.all())
    data_text = forms.CharField(required=False)
    data_file = forms.FileField(required=False)
    data_brief = forms.CharField(required=False)

    class Meta:
        fields = ['user', 'task_base', 'data_text', 'data_file', 'data_brief']
        model = UserDataTask


class UserTaskCommentForm(forms.ModelForm):
    user = forms.ModelChoiceField(required=True, queryset=User.objects.all())
    to_user = forms.ModelChoiceField(required=True, queryset=User.objects.all())
    task = forms.ModelChoiceField(required=True, queryset=TaskBase.objects.all())
    rank = forms.FloatField(required=True)
    comment = forms.CharField(required=True)

    class Meta:
        fields = ['user', 'to_user', 'task', 'rank', 'comment']
        model = UserTaskComment
