# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.forms import ModelForm, widgets
from django.forms import ModelChoiceField
from django.utils.translation import ugettext_lazy as _

from models import Task, DataTask, SubTask


###############################################################################
##        ModelForm                                                          ##
###############################################################################
class TaskForm(ModelForm):

    class Meta:
        model = Task
        # exclude = ['taskid', 'parenttask', 'views', 'submitnum', 'audituid']
        exclude = []
        widgets = {
            "task_model": widgets.Select(choices = (('1', '人工选择'), ('2', '自动选择'))),
            "trade_model": widgets.Select(choices = (('1', '悬赏'), ('2', '拍卖'))),
            "tender_model": widgets.Select(choices = (('0', '无'), ('1', '荷式'), ('2', '英式'))),
            "type_task": widgets.Select(choices = (('0', '无'), ('1', '单人'), ('2', '多人'))),
            "require_task": widgets.Select(choices = (('0', '无'), ('1', '全部'), ('2', '部分'))),
            "aim_task": widgets.Select(choices = (('0', '无'), ('1', '社会效益最大'), ('2', '发布者效益最大'), ('2', '带预算效益最大'))),
            "real_task": widgets.Select(choices = (('1', '要求'), ('2', '不要求'))),
            'describe': widgets.Textarea(attrs={'rows': 8}),
            'opinion': widgets.Textarea(attrs={'rows': 8}),
            'station': widgets.Select(choices = (('1', '待审核'), ('2', '待确认'), ('3', '已返回'), ('4', '报名中'), ('5', '完成'))),
        }
        
class SubTaskForm(ModelForm):
    class Meta:
        model = SubTask
        exclude = []
        widgets = {
            'subdescribe': widgets.Textarea(attrs={'rows': 8}),
        }
        
        
class DataTaskForm(ModelForm):
    class Meta:
        model = DataTask
        exclude = ['tid', 'uid', 'sid', "audit_uid", 'quality']
        widgets = {
            'databrief': widgets.Textarea(attrs={'rows': 4}),
            'station': widgets.Select(choices = (('3', '等待审核'), ('4', '需要修改'), ('5', '修改完成'), ('6', '数据确认')))
        }


###############################################################################
#########                  Filter                       #######################
###############################################################################

class TaskStatusFilter(admin.SimpleListFilter):
    title = _('任务状态')
    parameter_name  = 'station'
    def lookups(self, request, model_admin):
        return (
            ('1', _('待审核')),
            ('2', _('待确认')),
            ('3', _('已返回')),
            ('4', _('报名中')),
        )
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(station = self.value())


class TaskTypeFielter(admin.SimpleListFilter):
    title = _('任务类型')
    parameter_name = 'typetask'
    def lookups(self, requst, model_admin):
        return (
            ('1', _('单人')),
            ('2', _('多人')),
        )
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(typetask = self.value())


class TaskTradeModelFielter(admin.SimpleListFilter):
    title = _('交易模式')
    parameter_name = 'trademodel'
    def lookups(self, requst, model_admin):
        return (
            ('1', _('悬赏')),
            ('2', _('拍卖')),
        )
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(typetask = self.value())


class TaskIndustryFielter(admin.SimpleListFilter):
    title = _('所属行业')
    parameter_name = 'typetask'
    def lookups(self, requst, model_admin):
        return (
            ('1', _('单人')),
            ('2', _('多人')),
        )
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(typetask = self.value())


class SubTaskInline(admin.TabularInline):
    model = SubTask
    form = SubTaskForm


class DataStatusFielter(admin.SimpleListFilter):
    title = _('数据状态')
    parameter_name = 'datastatus'
    def lookups(self, requst, model_admin):
        return (
            ('3', _('待审核')),
            ('4', _('需要修改')),
            ('5', _('已经修改')),
        )
    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(station = self.value())


###############################################################################
########################            admin           ###########################
###############################################################################

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    inlines = [
        SubTaskInline,
    ]
    form = TaskForm
    list_display = ['name', 'describe', 'starttime', 'lasttime']
    list_filter = [TaskStatusFilter, TaskTypeFielter, TaskTradeModelFielter, TaskIndustryFielter]

    # override
    def save_model(self, request, obj, form, change):
        if change:
            obj.audituid = request.user.id
            obj.submitnum += 1
        obj.save()


@admin.register(DataTask)
class DataTaskAdmin(admin.ModelAdmin):
    form = DataTaskForm
    list_display = ['databrief', 'updatetime']
    list_filter = [DataStatusFielter, TaskTypeFielter, TaskTradeModelFielter, TaskIndustryFielter]

    def save_model(self, request, obj, form, change):
        if change:
            obj.audituid = request.user.id
            obj.submitnum += 1
        obj.save()
