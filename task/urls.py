from django.conf.urls import url
from . import views, task_check_api

urlpatterns = [
    url(r'^create$', views.create_task, name='create_task'),
    url(r'^get_list', views.get_list, name='get_list'),
    url(r'^task_detail', views.task_detail, name='task_detail'),
    url(r'^join_task', views.join_task, name='join_task'),
    url(r'^get_user_list', views.get_user_list, name='get_running_list'),
    url(r'^task_join_detail', views.task_join_detail, name='task_join_detail'),
    url(r'^get_publish_task', views.get_publish_task, name='get_publish_task'),
    url(r'^upload_task_data', views.upload_task_data, name='upload_task_data'),
    url(r'^update_user_data', views.update_user_data, name='update_user_data'),
    url(r'^task_join_user', views.task_join_user, name='task_join_user'),
    url(r'^agree_user_join', views.agree_user_join, name='agree_user_join'),
    url(r'^find_task_data', views.find_task_data, name='find_task_data'),
    url(r'^create_comment', views.create_comment, name='create_comment'),
    url(r'^into_upload_task', views.into_upload_task, name='into_upload_task'),
    url(r'^get_check_list', views.get_check_list, name='get_check_list'),
    url(r'^pass_check', views.pass_check, name='pass_check'),
    url(r'^prevent_check', views.prevent_check, name='prevent_check'),
    url(r'^get_data_list', views.get_data_list, name='get_data_list'),
    url(r'^pass_data', views.pass_data, name='pass_data'),
    url(r'^prevent_data', views.prevent_data, name='prevent_data'),

    url(r'^task_auto_check', task_check_api.task_check, name='taskCheckApi')
]
