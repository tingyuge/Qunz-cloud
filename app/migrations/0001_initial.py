# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TokenStore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token_key', models.CharField(default=b'', max_length=40)),
                ('token_data', models.TextField(max_length=2048)),
                ('token_expire', models.DateTimeField(null=True)),
            ],
        ),
    ]
