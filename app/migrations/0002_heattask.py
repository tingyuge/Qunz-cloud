# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0048_auto_20170428_2117'),
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HeatTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('heat_point', models.FloatField(default=0)),
                ('look_num', models.IntegerField(default=0)),
                ('join_num', models.IntegerField(default=0)),
                ('task_base', models.ForeignKey(to='task.TaskBase')),
            ],
        ),
    ]
