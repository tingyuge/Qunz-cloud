# coding=utf-8
from django.db import models
from task.models import TaskBase


class TokenStore(models.Model):
    token_key = models.CharField(max_length=40, default='')
    token_data = models.TextField(max_length=2048)
    token_expire = models.DateTimeField(null=True)


class HeatTask(models.Model):
    task_base = models.ForeignKey(TaskBase)
    # 热度
    heat_point = models.FloatField(null=False, default=0)
    # 查看次数
    look_num = models.IntegerField(null=False, default=0)
    # 参与人数
    join_num = models.IntegerField(null=False, default=0)
