# -*- coding: utf-8 -*-

from django.apps import AppConfig

class ActivityConfig(AppConfig):
    name = 'activity'
    verbose_name = "活动"