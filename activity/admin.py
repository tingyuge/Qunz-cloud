from django.contrib import admin
from django.forms import ModelForm, widgets

from models import Activity


# Register your models here.

class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        exclude = []
        widgets = {
            'describe': widgets.Textarea(attrs={'rows': 20})
        }
        

@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    form = ActivityForm