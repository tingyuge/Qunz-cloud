# coding:utf-8
from django.db import models

# Create your models here.

class Activity(models.Model):
    class Meta:
        verbose_name = '活动'
        verbose_name_plural = '活动'
    title = models.CharField('标题', null=False, max_length=200)
    start_time = models.DateTimeField('开始日期', null=False)
    end_time = models.DateTimeField('结束日期', null=False)
    describe = models.CharField('活动内容', null=False, max_length=2000)

    def __unicode__(self):
        return self.title
