#-*- coding=utf-8 -*-

import socket
import json
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import deal
'''
1.创建socket对象
2.socket绑定到指定地址
3.监听数据
4.接收客户端请求
5.处理阶段
6.传输结束
'''
socketServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socketServer.bind(('localhost', 8001))
socketServer.listen(5)

while True:
    connection, address = socketServer.accept()
    try:
        connection.settimeout(60)
        buf = connection.recv(1024)
        print buf
        bufdic = json.loads(buf)
        if "sensitiveWord" == bufdic["dealID"]:
            result0 = deal.dealSensitiveWord(bufdic["dealText"])
            connection.send(result0)
        elif "participle" == bufdic["dealID"]:
            result1 = deal.dealcut(bufdic["dealText"])
            connection.send(result1)
        elif "keyWord" == bufdic["dealID"]:
            result2 = deal.dealKeyWord(bufdic["dealText"])
            connection.send(result2)
        elif "partOfSpeech" == bufdic["dealID"]:
            result3 = deal.dealPartOfSpeech(bufdic["dealText"])
            connection.send(result3)
        elif "syntaxAnalysis" == bufdic["dealID"]:
            result4 = "语义提示"
            connection.send(result4)
        else:
            connection.send("没有相应的处理！！！")
    except socket.timeout:
        print 'time out'

    connection.close()
