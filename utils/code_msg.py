# coding=utf-8

codeMsg = {
    'login_success': {
        'code': 1364001001,
        'message': '登陆成功',
    },
    'register_success': {
        'code': 1364001002,
        'message': '注册成功',
    },
    'calculate_amount': {
        'code': 1364001003,
        'message': '计算成功',
    },
    'pay_success': {
        'code': 1364001004,
        'message': '支付成功',
    },


    'username_error': {
        'code': 1364010002,
        'message': '用户名密码错误',
    },
    'password_mot_match': {
        'code': 1364010003,
        'message': '确认密码不匹配',
    },
    'username_empty': {
        'code': 1364010004,
        'message': '用户名错误',
    },
    'register_error': {
        'code': 1364010005,
        'message': '注册失败',
    },
    'unique': {
        'code': 1364010006,
        'message': '已经存在',
    },
    'required': {
        'code': 1364010007,
        'message': '不能为空',
    },
    'task_join_error': {
        'code': 1364010008,
        'message': '报名任务失败',
    },
    'pay_error': {
        'code': 1364010009,
        'message': '支付失败',
    },
    'cash_error': {
        'code': 1364010010,
        'message': '账户余额不足',
    },
    'fare_error': {
        'code': 1364010012,
        'message': '提交金额错误',
    },

    'none': {
        'code': 1364010011,
        'message': '数据错误',
    }
}