短信验证使用http://sms.mob.com/#/sms
__ROOT__:http://180.209.64.49:8001/
注册：
    PATH:__ROOT__/user/register POST
    Request:
        username => 手机号或是邮箱
        nickname
        password
        password_verify
    response:
        {"status": "error", "msg": {"password": "not_match", "nickname": "required"}}
            required 字段必须，为空或没有
            not_match 密码与确认密码不匹配
            unique 已经存在相同帐号
            not_username 用户名格式错误或为空

登陆：
    path:__ROOT__/user/login POST
    request:
        username => 手机号或是邮箱
        password
    response:
        {"status": "success", "msg": [{"fields": {"favourit": null, "name": null, "phone": "18351925597", "nicklogo": "", "idnum": "123456789", "phone_verify": 0, "occupation": null, "sex": 0, "email_verify": 0, "third_party_account": null, "nickname": "tingyugetc", "birthday": null, "complete": 0, "create_date": "2016-03-14T09:33:11Z", "isreal": "verify/20160320/2cc9e884c923f43d27a4c47a42369541.jpg", "lasttime": "2016-03-23T13:08:20Z", "salt": "", "email": null, "reputation": 0.0}, "model": "people.user", "pk": 1}], "key": "shg30iouejxm5gtq8wd9fa6azxec282t"}
        注：
            key部分web、ios请无视
            android需要接受key，并将key放置在http header内组成Cookie: sessionid=****(key的值)对服务器发起请求

            favourit　=>　爱好
            name =>　姓名
            phone =>　手机
            email =>　邮箱
            nicklogo =>　头像
            phone_verify => 手机号是否验证
            occupation =>　职业
            sex =>　性别
            email_verify =>　邮箱是否验证
            third_party_account =>　第三方平台帐号
            nickname =>　昵称
            birthday =>　生日
            complete =>　完成任务数
            create_date =>　注册时间
            isreal =>　是否实名认证，实名认证为身份证路径
            lasttime =>　最后登陆时间
            reputation => 声誉

            model pk多余数据，无视

        {"status": "error", "msg": "错误原因"}
            wrong_username => 用户名或密码错误
            empty_username => 用户名或密码为空错误

实名认证:
    PATH:__ROOT__/user/verify POST
    request:
        isreal => 身份证正面
        photoback => 身份证反面
        idnum => 身份证号
    response:
        {"status": "error", "msg": {"idnum": "required", ****}}
            required => 字段不能为空
            not_login => 没有登录
            request_method => 禁止GET请求
        {"status": "success", "msg": ""}


忘记密码：
    PATH:__ROOT__/user/forgot
    request:
        username => 手机号或是邮箱
    response:
        {"status": "error", "msg": {"password": "not_match"}}
            not_match
            http_method


获得任务列表：
    PATH:__ROOT__/task/getListById POST
    request:
        lastid => 当前最后一条数据的id
        ordernum => 排序方式
                    排序方式(0智能排序，1酬劳最高，2离我最近，3最新上线)
                    首页的任务列表不用上传
        typeby => 任务类型
                    0智能筛选，1单人任务2多人任务
                    首页的任务列表不用上传

    response:
        {"status": "success", "msg": [{"icontask": "", "faretask": null, "describe": "***", "id": 1, "name": "***"}, {"icontask": "", "faretask": null, "describe": "***", "id": 2, "name": "***"}]}
            icontask => 任务头像
            faretask => 任务金额
            describe => 任务描述
            id => 任务编号
            name => 任务名称
            parenttask_id => 父任务id,如果没有为自身id
        {"status": "error", "msg": "***"}


获得任务详情：
    PATH:__ROOT__/task/taskDetail POST
    request:
        id => 任务id
    response:
        {"status": "success", "msg": [{"fields": {"submitnum": 1, "tendermodel": 1, "demo": "", "describe": "\u6536\u96c6\u5357\u4eac\u7684\u65b9\u8a00", "upperlimit": 100, "lastupdate": "2016-03-07T14:25:57Z", "templete": "", "faretask": null, "lasttime": "2016-03-07T14:25:57Z", "typetask": 1, "parenttask": null, "overtime": "2016-03-07T14:25:57Z", "station": 1, "trademodel": 1, "views": 0, "subtask": 0, "dataformat": "", "ownuser": 7, "address": "", "audituid": null, "realtask": 1, "name": "\u6536\u96c6\u5357\u4eac\u8bdd", "taskmodel": 1, "icontask": "", "industry": "\u8bed\u8a00", "aimtask": 1, "requiretask": 1, "peoplenum": 100, "starttime": "2016-03-07T14:25:57Z"}, "model": "task.task", "pk": 3}, {"userstation": 4}]}
            fields => 详情字段内容

                taskid => 任务编号
                ownuser => 发布者id
                taskmodel => 任务模式(不能为空，1人工选择，2为自动选择)
                trademodel => 交易模式(不能为空，1为悬赏，2为拍卖)
                peoplenum => 竞标人数(默认为1人)
                faretask => 任务金额(当交易模式为1时不为空，2时可以为空)悬赏具有
                taskfare => 押金(押金必须有)
                tradefare => 交易金额(支付了多少金额),必须有
                tendermodel => 招标形式(默认为0, 1荷2英)
                typetask => 任务类型(默认为0， 1单人2多人)
                requiretask => 任务要求(默认为0, 1全部2部分)
                aimtask => 任务目标(默认为0,1社会最大，2发布者最大，3带预算最大（当任务金额不为空时出现）)
                realtask => 用户实名要求(默认为0, 1要求， 2不要求)
                name => 任务名称
                describe => 任务描述
                icontask => 任务图标
                address => 任务地点，字段内容需要进行格式化
                lasttime => 报名截至时间
                starttime => 任务开始时间
                overtime => 任务结束时间
                upperlimit => 任务执行上限
                datatype => 数据类型(默认为0,1文本2图片3视频4音频5调查)
                dataformat =>
                # 数据格式 0所有
                    # 文本类:1word 2txt 3xml 4excel 5pdf
                    # 图片类:1vsd 2opj 3dwg 4bmp 5pcx 6tiff 7gif 8jpeg 9tag 10exif 11fpx 12svg
                    #         13psd 14cdr 15pcd 16dxf 17ufo 18eps 19ai 20png 21hdri 22raw
                    # 音频类;1cd 2wave 3aiff 4au 5mpeg 6mp3 7mpeg-4 8midi 9wma 10realaudio 11vqf
                    #         12oggvorbis 13amr
                    # 视频类:1wmv 2asf 3asx 4rm 5rmvb 6mpg 7mpeg 8mpe 9(3gp) 10mov 11mp4 12m4v
                    #         13avi 14dat 15mkv 16flv 17vob

                subtasknum => 子任务数
                templete => 模板位置
                demo => 样例位置
                station => 任务状态(1待审核2待确认3已反馈4报名中5执行中6完成)
                opinion => 审核意见
                views => 浏览数
                lastupdate => 最后更新时间
                submitnum => 任务提交次数
                audituid => 审核员id
                industry => 所属行业
                userstation => 报名者查看任务的报名状态
            pk => 父任务id,如果没有子任务则为自身id
            sid => 当前子任务id，如果不是子任务，则此项无

        {"status": "error", "msg": "***"}

报名任务：
    PATH:__ROOT__/task/taskJoin POST
    request:
        tid => 任务id
        uid => 用户id
        sid => 子任务id
    response:
        {"status": "success", "msg": ""}

获得正在执行任务的列表
    PATH:__ROOT__/task/getRunningListById POST
    request:
        uid
        lastid
    response:
        {"status": "success", "msg": [{"icontask": "task/demo.jpg", "jointime": "2016-03-16T10:52:17Z", "station": 1, "id": 1, "name": " \u6536\u96c6\u5317\u4eac\u8bdd"}]}
        icontask => 任务头像
        jointime => 开始时间
        station => 任务状态
            0报名， 1任务正在执行，报名已被发布者批准，需要提交数据
            2报名被发布者拒绝， 3数据等待审核
            4数据需要修改,  5任务完成，数据已经确认
        id => 任务id
        name => 任务名称

获得已经完成任务的列表
    PATH:__ROOT__/task/getFinshedListById POST
    request:
        uid
        lastid
    response:
        {"status": "success", "msg": [{"icontask": "task/demo.jpg", "jointime": "2016-03-16T10:52:17Z", "station": 1, "id": 1, "name": " \u6536\u96c6\u5317\u4eac\u8bdd"}]}
        icontask => 任务头像
        jointime => 开始时间
        station => 任务状态
            0报名， 1任务正在执行，报名已被发布者批准，需要提交数据
            2报名被发布者拒绝， 3数据等待审核
            4数据需要修改,  6任务完成，数据已经确认
        id => 任务id
        name => 任务名称

获得发布的任务列表
    PATH:__ROOT__/task/getPublishedListById POST
    request:
        uid
        lastid
    response:
        同任务详情

进入提交数据界面：
    PATH:__ROOT__/task/intoUploadData POST
    request:
        uid
        tid
    response:
        {"status": "success", "msg": {"faretask": null, "name": " \u6536\u96c6\u5317\u4eac\u8bdd", "icontask": "task/demo.jpg", "describe": "\u6536\u96c6\u5317\u4eac\u7684\u65b9\u8a00", "taskid": null, "jointime": "2016-03-16T10:52:17Z"}}
        faretask => 任务金额
        name => 任务名称
        icontask => 头像
        describe => 描述
        taskid => 任务单号
        jointime => 加入时间

提交任务数据/修改任务数据
    PATH:__ROOT__/task/uploadData POST
    request:
        uid
        tid => 父任务id
        sid => 子任务id，如若不是子任务此字段的值为tid的值
        datafile => 数据文件
        databrief => 数据说明
    response:
        {"status": "success", "msg": ''}
        {"status": "error", "msg": {}}

获得行业列表
    PATH:__ROOT__/setting/getIndustry POST
    request:

    response:
        {"status": "success", "msg": [{"fields": {"describe": "\u65b9\u8a00\u554a\u65b9\u8a00", "name": "\u65b9\u8a00"}, "model": "setting.industry", "pk": 1}, {"fields": {"describe": "\u5927\u6570\u636e\u884c\u4e1a", "name": "\u5927\u6570\u636e"}, "model": "setting.industry", "pk": 2}]}
        describe => 行业简介
        name => 行业名称

上传任务:
    PATH:__ROOT__/task/uploadTask POST
    request:
        uid => 发布者id
        ownuser => 发布者id
        taskmodel => 任务模式(不能为空，1人工选择，2为自动选择)
        trademodel =>　交易模式(不能为空，1为悬赏，2为拍卖)
        peoplenum =>　竞标人数(默认为1人)
        faretask => 任务金额
        tendermodel =>　招标形式(默认为0, 1荷2英)
        typetask =>　任务类型(默认为0， 1单人2多人)
        requiretask =>　任务要求(默认为0, 1全部2部分)
        aimtask =>　任务目标(默认为0,1社会最大，2发布者最大，3带预算最大（当任务金额不为空时出现）)
        realtask =>　用户实名要求(默认为0, 1要求， 2不要求)
        name =>　任务名称
        describe =>　任务描述
        industry =>　所属行业
                        通过getIndustry接口获得，上传行业id
        subdescribe => 子任务描述，格式化为json，为空上传[]
        subtasknum => 子任务数量(存在子任务则子任务数量大于1),没有子任务上传0
        datatype => 数据类型(默认为0,1文本2图片3视频4音频5调查)
        dataformat => 数据格式 0所有(如若没有具体选择上传0)
            文本类:1word 2txt 3xml 4excel 5pdf
            图片类:1vsd 2opj 3dwg 4bmp 5pcx 6tiff 7gif 8jpeg 9tag 10exif 11fpx 12svg
                    13psd 14cdr 15pcd 16dxf 17ufo 18eps 19ai 20png 21hdri 22raw
            音频类;1cd 2wave 3aiff 4au 5mpeg 6mp3 7mpeg-4 8midi 9wma 10realaudio 11vqf
                    12oggvorbis 13amr
            视频类:1wmv 2asf 3asx 4rm 5rmvb 6mpg 7mpeg 8mpe 9(3gp) 10mov 11mp4 12m4v
                    13avi 14dat 15mkv 16flv 17vob
            多个用','分割上传

        lasttime => 报名截至时间(所有必须),精确至毫秒
        starttime => 开始时间(所有必须),精确至毫秒
        overtime => 结束时间(所有必须),精确至毫秒
        icontask => 任务图标(所有必须),精确至毫秒
        address => 任务地点(所有必须),精确至毫秒
        demo => 样例
        template => 模板


        当任务为单人任务、悬赏形式时　任务金额、任务模式、交易模式、任务类型、用户实名要求、任务名称、任务描述、所属行业、数据类型、数据格式必须
                                    不存在子任务、任务要求、任务目标、招标形式、竞标人数
        当任务为单人任务、拍卖形式时　任务模式、交易模式、竞标人数、任务类型、用户实名要求、任务名称、任务描述、所属行业、数据类型、数据格式必须
                                不存在任务金额、子任务、任务要求、任务目标
                        当任务为自动选择时招标形式存在
        当任务为多人任务、悬赏形式时　任务金额、任务模式、交易模式、任务类型、任务要求、用户实名要求、任务名称、任务描述、所属行业、数据类型、数据格式必须
                                不存在任务目标、招标形式
                                可能存在子任务
        当任务为多人任务、拍卖形式时　任务模式、交易模式、任务类型、任务目标、任务要求、用户实名要求、任务名称、任务描述、所属行业、数据类型、数据格式必须
                                不存在任务金额、招标形式
                                可能存在子任务
    response:
        {"status": "success", "msg": ""}
        {"status": "error", "msg": ""}

获得报名用户：
    PATH:__ROOT__/task/getJoined POST
    request:
        uid
        tid
    response:
        {"status": "success", "msg": [{"joinname": "tingyugetc", "jointime": "2016-03-16T10:52:17Z", "joinuser": 1}]}
            jointime =>　报名时间
            joinname =>　报名用户昵称
            joinuser => 报名用户id
        {"status": "error", "msg": ""}

查看报名者详情：
    PATH:__ROOT__/task/getJoinDetail POST
    request:
        uid
        joinuid => 查看的用户id
    response:
        {"status": "success", "msg": {"nicklogo": "", "brief": null, "sex": 0, "reputation": 0.0, "isreal": true, "nickname": "tingyugetc"}}
            nicklogo
            brief => 个人简介
            sex
            reputation => 声誉
            isreal
            nickname
        {"status": "error", "msg": ""}
