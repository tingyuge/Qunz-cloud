# -*- coding: utf-8 -*-
from django.db import models

from people.models import *

# Create your models here.


class Question(models.Model):
    # 发布问卷的用户
    class Meta:
        verbose_name = '问卷'
        verbose_name_plural = '问卷'
    user = models.ForeignKey(User, verbose_name='用户')
    # 问卷内容
    content = models.CharField('内容', null=False, max_length=5000)
    # 问卷状态
    station = models.IntegerField('问卷状态',null=True, blank=True)

    def __unicode__(self):
        return "问卷"


class UserAnswer(models.Model):
    class Meta:
        verbose_name = '问卷答案'
        verbose_name_plural = '问卷答案'
    # 问卷回答者
    user = models.ForeignKey(User)
    # 问卷结果
    answer = models.CharField('答案',null=False, max_length=2000)
    # 关联问卷内容
    question = models.ForeignKey(Question)

    def __unicode__(self):
        return "问卷答案"