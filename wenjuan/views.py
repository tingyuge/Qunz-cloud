# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse


from common import functions
# Create your views here.


def uploadQuestionTemplate(request):
    uid = request.session.get('user_id')
    return render(request, 'question/uploadQuestionTemplate.html', {'uid': uid})


def uploadQuestion(request):
    isLogin = functions.isPostRight(request)
    if isLogin and request.method == 'POST':
        # 接受数据进行保存
        questionForm = modelform_factory(Question, form=QuestionForm)
        data = request.POST.copy()
        form = questionForm(data)
        if form.is_valid():
            print "success"
            msg = {'status': 'success', 'msg': ''}
        else:
            errors = functions.get_form_errors(form.errors)
            msg = {'status': 'error', 'msg': errors}
        return JsonResponse(msg)
    else:
        return JsonResponse(isLogin)


def getUserAnswer(request):
    isLogin = functions.isPostRight(request)
    if isLogin and request.method == 'POST':
        # 接受数据进行保存
        answerForm = modelform_factory(UserAnswer, form=AnswerForm)
        data = request.POST.copy()
        form = answerForm(data)
        if form.is_valid():
            print "success"
            msg = {'status': 'success', 'msg': ''}
        else:
            errors = functions.get_form_errors(form.errors)
            msg = {'status': 'error', 'msg': errors}
        return JsonResponse(msg)
    else:
        return JsonResponse(isLogin)
