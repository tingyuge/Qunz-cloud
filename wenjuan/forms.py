# -*- coding: utf-8 -*-
import datetime

from django import forms
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _

from .models import *

from people.models import User
from setting.models import Industry


class QuestionForm(forms.ModelForm):
    user = forms.ModelChoiceField(label=_('User'), queryset=User.objects.all())
    content = forms.CharField(required=True)

    class Meta:
        fields = [
            'user', 'content'
        ]


class AnswerForm(forms.ModelForm):
    user = forms.ModelChoiceField(label=_('User'), queryset=User.objects.all())
    answer = forms.CharField(max_length=2000)
    question = forms.ModelChoiceField(label=_('Question'), queryset==Question.objects.all())

    class Meta:
        fields = [
            'user', 'answer', 'question'
        ]
