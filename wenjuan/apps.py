# -*- coding: utf-8 -*-

from django.apps import AppConfig

class WenjuanConfig(AppConfig):
    name = 'wenjuan'
    verbose_name = "问卷"