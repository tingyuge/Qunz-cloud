# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wenjuan', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='question',
            options={'verbose_name': '\u95ee\u5377', 'verbose_name_plural': '\u95ee\u5377'},
        ),
        migrations.AlterModelOptions(
            name='useranswer',
            options={'verbose_name': '\u95ee\u5377\u7b54\u6848', 'verbose_name_plural': '\u95ee\u5377\u7b54\u6848'},
        ),
        migrations.AlterField(
            model_name='question',
            name='content',
            field=models.CharField(max_length=5000, verbose_name=b'\xe5\x86\x85\xe5\xae\xb9'),
        ),
        migrations.AlterField(
            model_name='question',
            name='station',
            field=models.IntegerField(null=True, verbose_name=b'\xe9\x97\xae\xe5\x8d\xb7\xe7\x8a\xb6\xe6\x80\x81', blank=True),
        ),
        migrations.AlterField(
            model_name='question',
            name='user',
            field=models.ForeignKey(verbose_name=b'\xe7\x94\xa8\xe6\x88\xb7', to='people.User'),
        ),
        migrations.AlterField(
            model_name='useranswer',
            name='answer',
            field=models.CharField(max_length=2000, verbose_name=b'\xe7\xad\x94\xe6\xa1\x88'),
        ),
    ]
