# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0008_auto_20160613_2223'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=5000)),
                ('station', models.IntegerField(null=True, blank=True)),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
        migrations.CreateModel(
            name='UserAnswer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=2000)),
                ('question', models.ForeignKey(to='wenjuan.Question')),
                ('user', models.ForeignKey(to='people.User')),
            ],
        ),
    ]
