# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import *
# Register your models here.


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ['user', 'question_title', 'question_station']

    def question_title(self, obj):
        return u"问卷标题"
    question_title.short_description = "问卷标题"

    def question_station(self, obj):
        return obj.station
    question_station.short_description = "问卷状态"
    