# -*- coding: utf-8 -*-
from django.conf.urls import url

import views


urlpatterns = [
    url(r'^uploadQuestionTemplate', views.uploadQuestionTemplate, name='uploadQuestionTemplate'),
    url(r'^uploadQuestion', views.uploadQuestion, name='uploadQuestion'),
]
