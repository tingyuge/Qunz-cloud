# coding=utf-8
from django.db import models

from people.models import User
from task.models import TaskBase, TaskExt

# Create your models here.


class TradeTask(models.Model):
    """
    用户交易记录表
    """
    trade_num = models.CharField('交易单号', null=False, max_length=255)
    pay_user = models.ForeignKey(User, related_name='pay_user', null=True)
    receive_user = models.ForeignKey(User, related_name='receive_user', null=True)
    # 0表示系统内支付, 1支付宝, 2微信
    pay_way = models.IntegerField('支付方式', null=False, default=0)
    task = models.ForeignKey(TaskBase)
    fare = models.FloatField('交易金额', null=False, default=0)
    remark = models.CharField('备注', blank=True, max_length=2048)
    # 订单状态, 0表示未支付，1表示完成了支付，2表示订单被取消关闭
    station = models.IntegerField('订单状态', null=False, default=0)
    create_time = models.DateTimeField('交易时间', auto_now_add=True)
    update_time = models.DateTimeField('支付时间', auto_now=True)

