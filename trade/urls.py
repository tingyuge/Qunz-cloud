from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'pay', views.pay_task, name='pay'),
    url(r'calculate_task', views.calculate_task, name='calculate_task'),
    url(r'grant_money', views.grant_money, name='grant_money'),
]
