# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0011_auto_20170428_2033'),
        ('task', '0050_auto_20170601_2312'),
    ]

    operations = [
        migrations.CreateModel(
            name='TradeTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('trade_num', models.CharField(max_length=255, verbose_name=b'\xe4\xba\xa4\xe6\x98\x93\xe5\x8d\x95\xe5\x8f\xb7')),
                ('pay_way', models.IntegerField(default=0, verbose_name=b'\xe6\x94\xaf\xe4\xbb\x98\xe6\x96\xb9\xe5\xbc\x8f')),
                ('fare', models.FloatField(default=0, verbose_name=b'\xe4\xba\xa4\xe6\x98\x93\xe9\x87\x91\xe9\xa2\x9d')),
                ('remark', models.CharField(max_length=2048, verbose_name=b'\xe5\xa4\x87\xe6\xb3\xa8', blank=True)),
                ('station', models.IntegerField(default=0, verbose_name=b'\xe8\xae\xa2\xe5\x8d\x95\xe7\x8a\xb6\xe6\x80\x81')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name=b'\xe4\xba\xa4\xe6\x98\x93\xe6\x97\xb6\xe9\x97\xb4')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name=b'\xe6\x94\xaf\xe4\xbb\x98\xe6\x97\xb6\xe9\x97\xb4')),
                ('pay_user', models.ForeignKey(related_name='pay_user', to='people.User', null=True)),
                ('receive_user', models.ForeignKey(related_name='receive_user', to='people.User', null=True)),
                ('task', models.ForeignKey(to='task.TaskBase')),
            ],
        ),
    ]
